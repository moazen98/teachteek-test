<?php

namespace App\Listeners;

use App\Providers\NotificationCustomerStoreEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotificationCustomerStoreListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Providers\NotificationCustomerStoreEvent  $event
     * @return void
     */
    public function handle(NotificationCustomerStoreEvent $event)
    {
        //
    }
}
