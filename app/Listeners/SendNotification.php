<?php

namespace App\Listeners;

use App\Event\NotificationCreated;
use App\Models\User;
use App\Models\UserNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Event\NotificationCreated $event
     * @return void
     */
    public function handle(NotificationCreated $event)
    {
        $users = User::where('active', 1)->get();
        $title = __('Caution for book quantity');
        $body = __('Book') . ' : ' . app()->getLocale() == 'ar' ? $event->book->book_name_ar : $event->book->book_name_en . ' ' . __('Minimum quantity has reached') . ' : ' . $event->book->minimum_quantity;
        foreach ($users as $user) {

            $notification = new UserNotification();
            $notification->title = $title;
            $notification->body = $body;
            $notification->is_read = 0;
            $notification->date_read = null;
            $notification->user()->associate($user);
            $notification->save();

        }
    }
}
