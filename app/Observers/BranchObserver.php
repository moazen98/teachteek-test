<?php

namespace App\Observers;

use App\Models\Agency\Branch;
use Illuminate\Support\Facades\DB;

class BranchObserver
{
    public function deleting(Branch $branch)
    {
        DB::beginTransaction();
        $branch->email = $branch->email .'-'.$branch->id.'-deleted';
        $branch->phone = $branch->phone .'-'.$branch->id.'-deleted';
        $branch->save();
        DB::commit();
    }
}
