<?php

namespace App\Observers;

use App\Models\Agency\Office;
use Illuminate\Support\Facades\DB;

class OfficeObserver
{

    public function deleting(Office $office)
    {
        DB::beginTransaction();
        $office->email = $office->email . '-' . $office->id . '-deleted';
        $office->phone = $office->phone . '-' . $office->id . '-deleted';
        $office->save();
        DB::commit();
    }
}
