<?php

namespace App\Observers;

use App\Models\Agency\Dealership;
use Illuminate\Support\Facades\DB;

class DealershipObserver
{
    public function deleting(Dealership $dealer)
    {
        DB::beginTransaction();
        $dealer->email = $dealer->email . '-' . $dealer->id . '-deleted';
        $dealer->phone = $dealer->phone . '-' . $dealer->id . '-deleted';
        $dealer->save();
        DB::commit();
    }
}
