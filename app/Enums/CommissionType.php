<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class CommissionType extends Enum
{
    const US =   1;
    const YOU =   2;
    const WITHOUT = 3;
}
