<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class MediaFor extends Enum
{
    const Employee = 'employee';
    const Publisher = 'publisher';
    const Customer = 'customer';
    const Author = 'author';
    const Book = 'book';
    const Distributor = 'distributor';
}
