<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class OffersType extends Enum
{
    const IN_PROGRESS =   1;
    const COMPLETE_INFORMATION =   2;
    const FINISHED = 3;
}
