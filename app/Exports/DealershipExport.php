<?php

namespace App\Exports;

use App\Models\Agency\Dealership;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DealershipExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $data = array();

        $users = Dealership::all();

        foreach ($users as $index => $user) {
            $data[$index]['id'] = $user->id;
            $data[$index]['name'] = $user->name;
            $data[$index]['email'] = $user->email;
            $data[$index]['pay_ratio'] = $user->pay_ratio;
            $data[$index]['phone'] = $user->international_code.$user->phone;
            $data[$index]['active'] = $user->is_active == 1 ? __("Active") : __("InActive");
            $data[$index]['register_date'] = $user->register_date;
            $data[$index]['last_login_date'] = $user->last_login_date == null ? __('Not login yet') : $user->last_login_date->format('m-d-Y');
            $data[$index]['date'] = $user->created_at->format('m-d-Y');
            $data[$index]['note'] = $user->note;
        }

        return $data;
    }

    public function headings(): array
    {
        return [__("Id"), __("Name"), __("Email"), __("Pay ratio"), __("Phone"), __("Status"), __("Register date"), __("Last login date"), __("Date"), __("Note")];
    }
}
