<?php

namespace App\Exports;

use App\Models\Customer;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustomerExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $data = array();

        $users = Customer::all();

        foreach ($users as $index => $user) {
            $data[$index]['id'] = ++$index;
            $data[$index]['first_name'] = $user->first_name;
            $data[$index]['last_name'] = $user->last_name;
            $data[$index]['job_title'] = $user->job_title;
            $data[$index]['phone'] = $user->phone;
            $data[$index]['email'] = $user->email;
            $data[$index]['address'] = $user->address;
            $data[$index]['note'] = $user->note;
            $data[$index]['active'] = $user->active == 1 ? __("Active") : __("InActive");
            $data[$index]['date'] = $user->created_at->format('m-d-Y');

        }

        return $data;
    }

    public function headings(): array
    {
        return [__("Id"), __("First Name"), __("Last Name"), __("Job Title"), __("Phone"), __("Email"), __("Address"), __("Note"), __("Status"), __("Date")];
    }
}
