<?php

namespace App\Exports;

use App\Models\Section;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SectionExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $data = array();

        $sections = Section::all();

        foreach ($sections as $index => $section) {
            $data[$index]['id'] = ++$index;
            $data[$index]['name_ar'] = $section->name_ar;
            $data[$index]['name_en'] = $section->name_en;
            $data[$index]['description'] = $section->description;
            $data[$index]['active'] = $section->active == 1 ? __("Active") : __("InActive");
            $data[$index]['date'] = $section->created_at->format('m-d-Y');
        }

        return $data;
    }

    public function headings(): array
    {
        return [__("Id"), __("Name Arabic"), __("Name English"), __("Description"),__("Status"),__("Date")];
    }
}
