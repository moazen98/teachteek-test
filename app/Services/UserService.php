<?php

namespace App\Services;

use App\Models\City;
use App\Models\Country;
use App\Models\Section\Section;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

/**
 * Class DealershipService.
 */
class UserService extends MainDashboardService
{

    public function getAllUsers($request)
    {

        $users = User::latest();

        return $users;
    }


    public function storeUser($request)
    {

        DB::beginTransaction();

        $employee = new User();
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->job_title = $request->job_title;
        $employee->phone = $request->full;
        $employee->email = $request->email;
        $employee->address = $request->address;
        $employee->note = $request->note;
        $employee->password = $request->password;
        $employee->gender = $request->gender;
        $employee->birthdate = $request->birthdate;

        if ($request->has('status')) {
            $employee->active = 1;
        } else {
            $employee->active = 0;
        }

        $section = Section::find($request->section);
        $employee->section()->associate($section);

        if ($request->has('country')) {
            $country = Country::find($request->country);
            $employee->country()->associate($country);
        }

        if ($request->has('city')) {
            $city = City::find($request->city);
            $employee->city()->associate($city);
        }

        $employee->setImage_urlAttributes($request);

        $employee->save();

        $role = Role::find($request->role);
        $employee->attachRole($role);

        DB::commit();

        return true;

    }

    public function updateUser($request, $id)
    {

        DB::beginTransaction();

        $employee = User::findOrFail($id);
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->job_title = $request->job_title;
        $employee->phone = $request->full;
        $employee->email = $request->email;
        $employee->address = $request->address;
        $employee->note = $request->note;
        $employee->gender = $request->gender;
        $employee->birthdate = $request->birthdate;

        if ($request->has('status')) {
            $employee->active = 1;
        } else {
            $employee->active = 0;
        }

        if (!is_null($request->password)) {
            $employee->password = $request->password;
        }

        $section = Section::find($request->section);
        $employee->section()->associate($section);

        if ($request->has('country')) {
            $country = Country::find($request->country);
            $employee->country()->associate($country);
        }

        if ($request->has('city')) {
            $city = City::find($request->city);
            $employee->city()->associate($city);
        }

        if ($request->has('image')) {
            $employee->setImage_urlAttributes($request);
        }

        $employee->save();

        $role = Role::find($request->role);
        foreach ($employee->roles as $employeeRole) {
            $employee->detachRole($employeeRole);
        }

        $employee->attachRole($role);


        DB::commit();

        return true;

    }



    public function getUserById($id)
    {
        $user = User::findOrFail($id);

        return $user;
    }

    public function destroyUserById($id)
    {

        DB::beginTransaction();

        $user = User::findOrFail($id);
        $user->delete();

        DB::commit();

        return true;
    }


        public function filterUsers($request)
    {

        $users = User::query();

        $dataTableFilter = $request->all();

        if ($dataTableFilter['country']) {

            $country = $request->get('country');

            $users->whereHas('country', function ($query) use ($country) {
                $query->where('id', '=', $country);
            });

        }


        if ($dataTableFilter['city']) {

            $city = $request->get('city');

            $users->whereHas('city', function ($query) use ($city) {
                $query->where('id', '=', $city);
            });

        }


        if ($dataTableFilter['query']) {
            $search = $request->get('query');
            $users->where(function ($query) use ($search) {
                $query->where('first_name', 'like', '%' . $search . '%')
                    ->orWhere('last_name', 'like', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%')
                    ->orWhere('phone', 'like', '%' . $search. '%');
            });
        }


        $orderBy = $dataTableFilter['sortby'] ? $dataTableFilter['sortby'] : 'DESC';

        $users = $users->latest()->orderBy('id', $orderBy);

        return $users;
    }


}
