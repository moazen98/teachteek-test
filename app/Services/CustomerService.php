<?php

namespace App\Services;

use App\Models\City;
use App\Models\Country;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;

/**
 * Class CustomerService.
 */
class CustomerService extends MainDashboardService
{


    public function getAllUsers($request)
    {

        $users = Customer::latest();

        return $users;
    }


    public function storeUser($request)
    {
        try {


            DB::beginTransaction();

            $customer = new Customer();
            $customer->first_name = $request->first_name;
            $customer->last_name = $request->last_name;
            $customer->job_title = $request->job_title;
            $customer->phone = $request->full;
            $customer->email = $request->email;
            $customer->address = $request->address;
            $customer->note = $request->note;
            $customer->password = $request->password;
            $customer->gender = $request->gender;
            $customer->birthdate = $request->birthdate;
            $customer->active = $request->has('status') ? 1 : 0;

            if ($request->has('country')) {
                $country = Country::find($request->country);
                $customer->country()->associate($country);
            }

            if ($request->has('city')) {
                $city = City::find($request->city);
                $customer->city()->associate($city);
            }

            $customer->setImage_urlAttributes($request);

            $customer->save();

            DB::commit();

            return $customer->id;

        } catch (\Exception $exception) {
            DB::rollBack();
            return false;
        }

    }

    public function updateUser($request, $id)
    {

        DB::beginTransaction();

        $customer = Customer::findOrFail($id);
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->job_title = $request->job_title;
        $customer->phone = $request->full;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->note = $request->note;
        $customer->password = $request->password;
        $customer->gender = $request->gender;
        $customer->birthdate = $request->birthdate;
        $customer->active = $request->has('status') ? 1 : 0;

        if ($request->has('image')) {
            $customer->setImage_urlAttributes($request);
        }

        $customer->save();

        DB::commit();

        return true;

    }


    public function getUserById($id)
    {
        $user = Customer::findOrFail($id);

        return $user;
    }

    public function destroyUserById($id)
    {

        DB::beginTransaction();

        $user = Customer::findOrFail($id);
        $user->delete();

        DB::commit();

        return true;
    }


    public function filterUsers($request)
    {

        $users = Customer::query();

        $dataTableFilter = $request->all();

        if ($dataTableFilter['country']) {

            $country = $request->get('country');

            $users->whereHas('country', function ($query) use ($country) {
                $query->where('id', '=', $country);
            });

        }


        if ($dataTableFilter['city']) {

            $city = $request->get('city');

            $users->whereHas('city', function ($query) use ($city) {
                $query->where('id', '=', $city);
            });

        }


        if ($dataTableFilter['query']) {
            $search = $request->get('query');
            $users->where(function ($query) use ($search) {
                $query->where('first_name', 'like', '%' . $search . '%')
                    ->orWhere('last_name', 'like', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%')
                    ->orWhere('phone', 'like', '%' . $search . '%');
            });
        }


        $orderBy = $dataTableFilter['sortby'] ? $dataTableFilter['sortby'] : 'DESC';

        $users = $users->latest()->orderBy('id', $orderBy);

        return $users;
    }


}
