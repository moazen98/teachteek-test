<?php

namespace App\Services;

/**
 * Class KernelServices.
 */
class KernelServices
{

    public $userService;
    public $locationService;
    public $customerService;
    public $blogService;


    /**
     * KernelServices constructor.
     */
    public function __construct()
    {
        $this->userService = new UserService();
        $this->customerService = new CustomerService();
        $this->locationService = new LocationService();
        $this->blogService = new BlogService();
    }
}
