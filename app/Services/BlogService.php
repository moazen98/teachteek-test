<?php

namespace App\Services;

use App\Models\Blog;
use App\Models\BlogImage;
use App\Models\City;
use App\Models\Country;
use App\Models\Section\Section;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class BlogService.
 */
class BlogService extends MainDashboardService
{



    public function getAllBlogs(){
        $blogs = Blog::all();
        return $blogs;
    }

    public function findBlog($id){
        $blog = Blog::findOrFail($id);
        return $blog;
    }

    public function storeBlog($request)
    {
        try {



        $id = Auth::guard('customers')->user()->id;
        $customer = app('servicesV1')->customerService->getUserById($id);

        DB::beginTransaction();

        $blog = new Blog();
        $blog->description = $request->description;
        $blog->customer()->associate($customer);
        $blog->save();

        foreach ($request->image as $imageId) {
            $image = new BlogImage();
            $image->image_id = $imageId;
            $image->blog()->associate($blog);
            $image->save();
        }

        DB::commit();

        return true;

        }catch (\Exception $exception){

            DB::rollBack();

            return false;
        }
    }

    public function filterBlogs($request)
    {

        $blogs = Blog::query();

        $dataTableFilter = $request->all();


        if ($dataTableFilter['query']) {
            $search = $request->get('query');
            $blogs->where(function ($query) use ($search) {
                $query->where('id', 'like', '%' . $search . '%')
                    ->orWhere('description', 'like', '%' . $search . '%');
            });
        }


        $orderBy = 'DESC';

        $blogs = $blogs->latest()->orderBy('id', $orderBy);

        return $blogs;
    }

}
