<?php

namespace App\Services;

use App\Models\City;
use App\Models\Country;

/**
 * Class LocationService.
 */
class LocationService extends MainDashboardService
{

    public function getAllCountries($request = null){

        $countries = Country::has('cities')->get();

        return $countries;
    }

    public function getCitiesByCountryId($id){
        $cites = array();
        $cites = City::where('country_id', '=',$id)->get();

        return $cites;
    }

}
