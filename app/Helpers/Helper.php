<?php


use App\Enums\AgencyType;
use App\Enums\MediaExtension;
use App\Enums\OffersType;
use App\Enums\OperationStatusType as OperationStatusTypeAlias;
use App\Models\Agency\Branch;
use App\Models\Agency\Dealership;
use App\Models\Agency\Office;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use \App\Http\Resources\Dashboard\Agency\User\UserResource;
use \App\Http\Resources\Dashboard\Agency\Dealer\DealerResource;
use \App\Http\Resources\Dashboard\Agency\Branch\BranchResource;
use \App\Http\Resources\Dashboard\Agency\Office\OfficeResource;

function upload_files($upload_path, $folder, $allowed_types = 'jpeg|jpg|png', $request)
{


    $ret_files = array();

    $upload_path = $upload_path . $folder;


    $config['upload_path'] = $upload_path;


    if (!file_exists($upload_path)) {
        File::makeDirectory(public_path() . '/' . $upload_path, 0777, true);

//        touch($upload_path . DIRECTORY_SEPARATOR . 'index.html');
    }

    $uploaded_file = null;

    foreach ($request->image as $file_key => $file) {

        $imageName = $file->getClientOriginalName();

        $uploaded_file[$file_key] = $file->move(public_path($upload_path), $imageName);
    }

    $res = is_null($uploaded_file);

    if ($res != true) {

        foreach ($request->image as $file_key => $file) {

            $ret_files[$file_key]['file_name'] = $file->getClientOriginalName();
            $ret_files[$file_key]['file_type'] = $file->getClientOriginalExtension();

            $imageName = $file->getClientOriginalName();

            $ret_files[$file_key]['real_path'] = $folder . '/' . $imageName;

        }

    } else {
        $ret_files['file_key'] = 'error upload';
        $response['status'] = false;
        $response['ret_data'] = array();

        return $response;
    }

    return $ret_files;
}


function upload_icons($upload_path, $folder, $allowed_types = 'jpeg|jpg|png', $request)
{


    $ret_files = array();

    $upload_path = $upload_path . $folder;


    $config['upload_path'] = $upload_path;


    if (!file_exists($upload_path)) {
        File::makeDirectory(public_path() . '/' . $upload_path, 0777, true);

//        touch($upload_path . DIRECTORY_SEPARATOR . 'index.html');
    }

    $uploaded_file = null;

    foreach ($request->icon as $file_key => $file) {

        $imageName = $file->getClientOriginalName();

        $uploaded_file[$file_key] = $file->move(public_path($upload_path), $imageName);
    }

    $res = is_null($uploaded_file);

    if ($res != true) {

        foreach ($request->icon as $file_key => $file) {

            $ret_files[$file_key]['file_name'] = $file->getClientOriginalName();
            $ret_files[$file_key]['file_type'] = $file->getClientOriginalExtension();

            $imageName = $file->getClientOriginalName();

            $ret_files[$file_key]['real_path'] = $folder . '/' . $imageName;

        }

    } else {
        $ret_files['file_key'] = 'error upload';
        $response['status'] = false;
        $response['ret_data'] = array();

        return $response;
    }

    return $ret_files;
}


function upload_filesServicePrescriptions($upload_path, $folder, $allowed_types = 'jpeg|jpg|png', $type, $servicePrescriptionId, $request)
{

    $ret_files = array();

    $upload_path = $upload_path . $folder;


    $config['upload_path'] = $upload_path;

    if (!file_exists($upload_path)) {
        File::makeDirectory(public_path() . '/' . $upload_path, 0777, true);
    }


    $full_path = $upload_path;

    if ($type == 'category') {
        if (!file_exists($upload_path . '/' . 'category') && !file_exists(public_path() . '/' . $upload_path . '/' . 'category')) {

            File::makeDirectory(public_path() . '/' . $upload_path . '/' . 'category', 0777, true);

        }
        $full_path .= '/' . 'category';
    }


    if ($type == 'item') {
        if (!file_exists($upload_path . '/' . 'item') && !file_exists(public_path() . '/' . $upload_path . '/' . 'item')) {
            File::makeDirectory(public_path() . '/' . $upload_path . '/' . 'item', 0777, true);
        }
        $full_path .= '/' . 'item';
    }

    if ($type == 'category') {
        if (!file_exists($upload_path . '/' . 'category' . '/' . 'service_prescription_' . $servicePrescriptionId) && !file_exists(public_path() . '/' . $upload_path . '/' . 'category' . '/' . 'service_prescription_' . $servicePrescriptionId)) {
            File::makeDirectory(public_path() . '/' . $upload_path . '/' . 'category' . '/' . 'service_prescription_' . $servicePrescriptionId, 0777, true);
        }
        $full_path .= '/' . 'service_prescription_' . $servicePrescriptionId;
    }

    if ($type == 'item') {
        if (!file_exists($upload_path . '/' . 'item' . '/' . 'service_prescription_' . $servicePrescriptionId) && !file_exists(public_path() . '/' . $upload_path . '/' . 'item' . '/' . 'service_prescription_' . $servicePrescriptionId)) {
            File::makeDirectory(public_path() . '/' . $upload_path . '/' . 'item' . '/' . 'service_prescription_' . $servicePrescriptionId, 0777, true);
        }
        $full_path .= '/' . 'service_prescription_' . $servicePrescriptionId;
    }

    $uploaded_file = null;

    foreach ($request->image as $file_key => $file) {

        $imageName = $file->getClientOriginalName();

        $uploaded_file[$file_key] = $file->move(public_path($full_path), $imageName);
    }

    $res = is_null($uploaded_file);

    if ($res != true) {

        foreach ($request->image as $file_key => $file) {

            $ret_files[$file_key]['file_name'] = $file->getClientOriginalName();
            $ret_files[$file_key]['file_type'] = $file->getClientOriginalExtension();

            $imageName = $file->getClientOriginalName();


            $ret_files[$file_key]['real_path'] = $full_path . '/' . $imageName;

        }

    } else {
        $ret_files['file_key'] = 'error upload';
        $response['status'] = false;
        $response['ret_data'] = array();

        return $response;
    }

    return $ret_files;
}


function upload_single_files($upload_path, $folder, $allowed_types = 'png|jpg|jpeg|pdf|xls', $request)
{


    $size = $request->getSize();
    $ret_files = array();

    $upload_path = $upload_path . $folder;


    $config['upload_path'] = $upload_path;


    if (!file_exists($upload_path)) {
        File::makeDirectory(public_path() . '/' . $upload_path, 0777, true);

    }

    $uploaded_file = null;

    $imageName = $request->getClientOriginalName();
    $imageNameHash = $request->hashName();
    $imageExtension = $request->getClientOriginalExtension();

    $uploaded_file[0] = $request->move(public_path($upload_path), $imageNameHash);

    $res = is_null($uploaded_file);

    if ($res != true) {

        $ret_files[0]['size'] = formatBytes($size);
        $ret_files[0]['file_name'] = $imageNameHash;
        $ret_files[0]['real_name'] = $imageName;
        $ret_files[0]['file_type'] = $imageExtension;
        $ret_files[0]['real_path'] = $folder . '/' . $imageName;


    } else {
        $ret_files['file_key'] = 'error upload';
        $response['status'] = false;
        $response['ret_data'] = array();

        return $response;
    }

    return $ret_files;
}

function week_of_month()
{

    return ceil(Carbon::now()->month(Carbon::now()->month)->daysInMonth / 7);
}

function getWeekday($date)
{
    return ceil(date('d', strtotime($date)) / 7);
}


function mobile_user()
{
    return null;
}


function formatBytes($bytes, $precision = 2)
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB');


    $bytes = max($bytes, 0);
    $pow = ceil($bytes / 1048576);

    // Uncomment one of the following alternatives
    // $bytes /= pow(1024, $pow);
    // $bytes /= (1 << (10 * $pow));

//    return round($bytes, $precision) . ' ' . $units[$pow];
    return round($pow, $precision);
}


function requestConvert($request)
{
    $requestConvert = new Request();
    $requestConvert = $request;

    return $requestConvert;
}

function image_extensions()
{

    $extensions_images = array();
    $extensions_images = MediaExtension::getValues(['PNG', 'JPEG', 'JPG', 'GIF']);

    return $extensions_images;
}


function files_extensions()
{

    $extensions_files = array();
    $extensions_files = MediaExtension::getValues(['XLS', 'PDF']);

    return $extensions_files;
}

function audio_extensions()
{

    $extensions_audio = array();
    $extensions_audio = MediaExtension::getValues(['MP3', 'OGG', 'WAV']);

    return $extensions_audio;
}

function vedio_extensions()
{

    $extensions_vedio = array();
    $extensions_vedio = MediaExtension::getValues(['MP4', 'MPEG', 'MPGA']);

    return $extensions_vedio;
}

//TODO : put other users when you create them
function getCurrentDashboardUser()
{

    $user = \Illuminate\Support\Facades\Auth::user();
    return $user;

}


function checkIfAvailableOperation($received)
{

    if ($received) {
        return false;
    }
    return true;

}

function getUserAgencyType($type)
{

    if ($type == User::class) {
        return AgencyType::MONTANA;
    }

    if ($type == Office::class) {
        return AgencyType::OFFICE;
    }

    if ($type == Branch::class) {
        return AgencyType::BRANCH;
    }

    if ($type == Dealership::class) {
        return AgencyType::DEALERSHIP;
    }

    return null;
}


function getOperationStatus($type)
{

    if ($type == 1) {
        return AgencyType::MONTANA;
    }

    if ($type == Office::class) {
        return AgencyType::OFFICE;
    }

    if ($type == Branch::class) {
        return AgencyType::BRANCH;
    }

    if ($type == Dealership::class) {
        return AgencyType::DEALERSHIP;
    }

    return null;
}


function getUserAgencyTypeString($type)
{

    if ($type == User::class) {
        return __('Montana');
    }

    if ($type == Office::class) {
        return __('Office');
    }

    if ($type == Branch::class) {
        return __('Branch');
    }

    if ($type == Dealership::class) {
        return __('Dealership');
    }

    return null;
}

function getOperationStatusString($type)
{

    if (OperationStatusTypeAlias::IN_PROGRESS == $type) {
        return "warning";
    }

    if (OperationStatusTypeAlias::APPROVED == $type) {
        return "info";
    }


    if (OperationStatusTypeAlias::FINISHED == $type) {
        return "success";
    }

    return null;
}


function getOfferStatusString($type)
{

    if (OffersType::IN_PROGRESS == $type) {
        return "warning";
    }

    if (OffersType::COMPLETE_INFORMATION == $type) {
        return "info";
    }


    if (OffersType::FINISHED == $type) {
        return "success";
    }

    return null;
}

function getOperationStatusCheckDelete($type)
{

    if (OperationStatusTypeAlias::IN_PROGRESS == $type) {
        return true;
    }

    if (OperationStatusTypeAlias::APPROVED == $type) {
        return false;
    }


    if (OperationStatusTypeAlias::FINISHED == $type) {
        return false;
    }

    return false;
}

function getOfferStatusCheckDelete($type)
{

    if (OffersType::IN_PROGRESS == $type) {
        return true;
    }

    if (OffersType::COMPLETE_INFORMATION == $type) {
        return false;
    }


    if (OffersType::FINISHED == $type) {
        return false;
    }

    return false;
}

function getOfferStatusCheckEdit($type)
{

    if (OffersType::IN_PROGRESS == $type) {
        return true;
    }

    if (OffersType::COMPLETE_INFORMATION == $type) {
        return false;
    }


    if (OffersType::FINISHED == $type) {
        return false;
    }

    return false;
}


function getOfferTypeString($type)
{

    if (OffersType::IN_PROGRESS == $type) {
        return __('In Progress');
    }

    if (OffersType::COMPLETE_INFORMATION == $type) {
        return __('Complete information');
    }


    if (OffersType::FINISHED == $type) {
        return __('Finished');
    }

    return __('No Data');
}


function getStatusString($obj)
{

    $string = $obj == null ? __('No data') : (app()->getLocale() == 'ar' ? $obj->name_ar : (app()->getLocale() == 'en' ? $obj->name_en : $obj->name_tr));

    return $string;
}




function getOperationStatusCheckEdit($type)
{

    if (OperationStatusTypeAlias::IN_PROGRESS == $type) {
        return true;
    }

    if (OperationStatusTypeAlias::APPROVED == $type) {
        return false;
    }


    if (OperationStatusTypeAlias::FINISHED == $type) {
        return false;
    }

    return false;
}

function getUserAgencyResource($type, $user)
{

    if ($type == User::class) {
        return (new UserResource($user))->toArray();
    }

    if ($type == Office::class) {
        return (new OfficeResource($user))->toArray();
    }

    if ($type == Branch::class) {
        return (new BranchResource($user))->toArray();
    }

    if ($type == Dealership::class) {
        return (new DealerResource($user))->toArray();
    }

    return null;
}
