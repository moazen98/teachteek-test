<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // available language in template array
        $availLocale=['ar'=>'ar','en'=>'en', 'fr'=>'fr','de'=>'de','pt'=>'pt'];

        // Locale is enabled and allowed to be change
        if(session()->has('locale') && array_key_exists(session()->get('locale'),$availLocale)){

            // Set the Laravel locale
            app()->setLocale(session()->get('locale'));

            if (session()->get('locale') == 'ar') {
                config(['MIX_CONTENT_DIRECTION' => 'rtl']);
                session()->put('direction', 'rtl');
                // dd(session()->get('direction'));
            }else {
                config(['MIX_CONTENT_DIRECTION' => 'ltr']);
                session()->put('direction', 'ltr');
                // dd(session()->get('direction'));
            }

        }

        return $next($request);

    }
}
