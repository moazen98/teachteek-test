<?php

namespace App\Http\Middleware;

use App\Enums\SectionType;
use App\Models\Section;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PreventUserAccessOtherSection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next,$section,$human_resource=null)
    {
        $user = Auth::user();
        $userSection = $user->section->type;


        if ($userSection == SectionType::HUMAN_RESOURCE){

            if ($userSection != $human_resource){
                Auth::logout();
                return redirect()->route('website.get.login')
                    ->with('failed', 'حسابك غير مصرح به الدخول إلى هذا القسم');
            }

            $section = Section::where('type','=',$user->section->type)->first();

            if ($section->active == 0){
                Auth::logout();
                return redirect()->route('website.get.login')
                    ->with('failed', 'القسم غير مفعل');
            }

            return $next($request);
        }


        if ($userSection != $section){
            Auth::logout();
            return redirect()->route('website.get.login')
                ->with('failed', 'حسابك غير مصرح به الدخول إلى هذا القسم');
        }

        $section = Section::where('type','=',$user->section->type)->first();

        if ($section->active == 0){
            Auth::logout();
            return redirect()->route('website.get.login')
                ->with('failed', 'القسم غير مفعل');
        }

        return $next($request);
    }
}
