<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GifResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'url' => $this->images->original->url,
            'slug' => $this->slug,
            'type' => $this->type,
        ];
    }
}
