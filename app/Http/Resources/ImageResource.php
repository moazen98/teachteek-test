<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Giphy;


class ImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'blog_id' => $this->blog_id,
            'url' => Giphy::getByID($this->image_id)->data->images->original->url,
        ];
    }
}
