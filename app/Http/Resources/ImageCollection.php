<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Giphy;

class ImageCollection extends ResourceCollection
{
    public function toArray($request=null)
    {
        return [
            'images' => $this->collection->map(function ($image) use ($request) {
                return (new ImageResource($image))->toArray($request);
            })
        ];
    }

}
