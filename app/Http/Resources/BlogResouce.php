<?php

namespace App\Http\Resources;

use App\Http\Resources\Dashboard\User\CustomerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BlogResouce extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'customer' => $this->customer == null ? null : (new CustomerResource($this->customer))->toArray(),
            'description' => $this->description,
            'images' => $this->images()->count() == 0 ? null : (new ImageCollection($this->images))->toArray(),
            'image_number' => $this->images()->count()
        ];
    }
}
