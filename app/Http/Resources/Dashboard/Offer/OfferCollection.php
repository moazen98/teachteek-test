<?php

namespace App\Http\Resources\Dashboard\Offer;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OfferCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'offers' => $this->collection->map(function ($offer) use ($request) {
                return (new OfferResource($offer))->toArray($request);
            })
        ];
    }
}
