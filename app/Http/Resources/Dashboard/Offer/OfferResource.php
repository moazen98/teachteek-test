<?php

namespace App\Http\Resources\Dashboard\Offer;

use App\Http\Resources\Dashboard\Currency\CurrencyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request = null)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'type_string' => getOfferTypeString($this->type),
            'commission_value' => $this->commission == null ? null : $this->commission->value,
            'commission_string' => getStatusString($this->commission),
            'currency_id' => $this->currencyFrom == null ? null : (new CurrencyResource($this->currencyFrom))->toArray(),
            'pay' => $this->pay,
            'amount' => $this->amount,
            'note' => $this->note,
            'user_type' => getUserAgencyType($this->receivedable_type),
            'user_type_string' => getUserAgencyTypeString($this->receivedable_type),
            'user' => $this->receivedable == null ? null : getUserAgencyResource($this->receivedable_type, $this->receivedable),
            'date' => $this->created_at->format('y-m-d H:i'),
            'country' => $this->country == null ? null : (app()->getLocale() == 'ar' ? $this->country->name_ar : $this->country->name_en),
            'country_id' => $this->country == null ? null : $this->country->id,
            'city' => $this->city == null ? null : (app()->getLocale() == 'ar' ? $this->city->name_ar : $this->city->name_en),
            'city_id' => $this->city == null ? null : $this->city->id,
            'status' => $this->offerStatus == null ? null : getStatusString($this->offerStatus),
            'status_value' => $this->operationStatus == null ? null : $this->offerStatus->value,
            'status_class' => $this->operationStatus == null ? null : getOfferStatusString($this->offerStatus->value),
            'is_can_delete' => $this->operationStatus == null ? false : getOfferStatusCheckDelete($this->offerStatus->value),
            'is_can_edit' => $this->operationStatus == null ? false : getOfferStatusCheckEdit($this->offerStatus->value),
            'received' => $this->receivedable == null ? null : getUserAgencyResource($this->receivedable_type, $this->receivedable),

        ];
    }
}
