<?php

namespace App\Http\Resources\Dashboard\Agency\Branch;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BranchCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'branches' => $this->collection->map(function ($branch) use ($request) {
                return (new BranchResource($branch))->toArray($request);
            })
        ];
    }
}
