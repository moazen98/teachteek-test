<?php

namespace App\Http\Resources\Dashboard\Agency\Branch\File;

use App\Enums\MediaSizeUnit;
use App\Models\MediaExtension;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchFilesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'url' => $this->file_path,
            'extension' => MediaExtension::find($this->extension_id) == null ? MediaExtension::first()->extension : MediaExtension::find($this->extension_id)->extension,
            'size' => $this->size,
            'size_unit' => MediaSizeUnit::MEGA,
            'real_name' => $this->real_name,
        ];
    }
}
