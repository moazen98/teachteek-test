<?php

namespace App\Http\Resources\Dashboard\Agency\Dealer;

use App\Http\Resources\Dashboard\Agency\Branch\BranchSettingResource;
use App\Http\Resources\Dashboard\Agency\Branch\File\BranchFilesCollection;
use App\Http\Resources\Dashboard\Agency\Dealer\File\DealerFilesCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class DealerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'pay_ratio' => $this->pay_ratio,
            'international_code' => $this->international_code,
            'full_phone' => $this->international_code . $this->phone,
            'is_active' => $this->is_active,
            'register_date' => $this->register_date,
            'address' => $this->address,
            'last_register_date' => $this->last_login_date == null ? null : $this->last_login_date->format('Y-m-d H:i'),
            'last_register_date_string' => $this->last_login_date == null ? __('Not login yet') : $this->last_login_date->format('Y-m-d H:i'),
            'note' => $this->note,
            'country' => $this->country == null ? null : (app()->getLocale() == 'ar' ? $this->country->name_ar : $this->country->name_en),
            'country_id' => $this->country == null ? null : $this->country->id,
            'city' => $this->city == null ? null : (app()->getLocale() == 'ar' ? $this->city->name_ar : $this->city->name_en),
            'city_id' => $this->city == null ? null : $this->city->id,
            'image_path' => $this->image_path,
            'setting' => $this->setting == null ? null : (new DealerSettingResource($this->setting))->toArray(),
            'files' => $this->files()->count() == 0  ? null : (new DealerFilesCollection($this->files))->toArray(),
            'user_type' => getUserAgencyType($this),
            'user_type_string' => getUserAgencyTypeString($this),
        ];
    }
}
