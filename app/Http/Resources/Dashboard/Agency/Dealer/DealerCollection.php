<?php

namespace App\Http\Resources\Dashboard\Agency\Dealer;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DealerCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'dealers' => $this->collection->map(function ($dealer) use ($request) {
                return (new DealerResource($dealer))->toArray($request);
            })
        ];
    }
}
