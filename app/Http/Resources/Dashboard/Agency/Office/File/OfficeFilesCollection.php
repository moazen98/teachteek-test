<?php

namespace App\Http\Resources\Dashboard\Agency\Office\File;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OfficeFilesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'files' => $this->collection->map(function ($file) use ($request) {
                return (new OfficeFilesResource($file))->toArray($request);
            })
        ];
    }
}
