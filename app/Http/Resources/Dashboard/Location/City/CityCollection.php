<?php

namespace App\Http\Resources\Dashboard\Location\City;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CityCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'cities' => $this->collection->map(function ($city) use ($request) {
                return (new CityResource($city))->toArray($request);
            })
        ];
    }
}
