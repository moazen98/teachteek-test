<?php

namespace App\Http\Resources\Dashboard\Location\City;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Session;

class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        $lang = mobile_user() == null ? app()->getLocale() : (Session::get('language') == null ? app()->getLocale() : Session::get('language'));

        return [
            'id' => $this->id,
            'name' => $this['name_' . $lang],
        ];
    }
}
