<?php

namespace App\Http\Resources\Dashboard\Section;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Session;

class SectionRescourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $lang = mobile_user() == null ? app()->getLocale() : (Session::get('language') == null ? app()->getLocale() : Session::get('language'));

        return [
            'id' => $this->id,
            'name' => $this->translate($lang)->name,
            'description' => $this->description,
            'active' => $this->active,
            'image_url' => $this->image_path,
        ];
    }
}
