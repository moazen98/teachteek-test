<?php

namespace App\Http\Resources\Dashboard\Operation;

use App\Enums\AgencyType;
use App\Http\Resources\Dashboard\Currency\CurrencyResource;
use App\Models\Agency\Branch;
use App\Models\Agency\Dealership;
use App\Models\Agency\Office;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class OperationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request = null)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'type_string' => $this->type == 1 ? __('Sale') : __('Buy'),
            'amount' => $this->amount,
            'currency_from_id' => $this->currencyFrom == null ? null : (new CurrencyResource($this->currencyFrom))->toArray(),
            'exchange_rate' => $this->exchange_rate,
            'currency_to_id' => $this->currencyTo == null ? null : (new CurrencyResource($this->currencyTo))->toArray(),
            'note' => $this->note,
            'user_type' => getUserAgencyType($this->operationable_type),
            'user_type_string' => getUserAgencyTypeString($this->operationable_type),
            'user' => $this->operationable == null ? null : getUserAgencyResource($this->operationable_type, $this->operationable),
            'date' => $this->created_at->format('y-m-d H:i'),
            'status' => $this->operationStatus == null ? null : (app()->getLocale() == 'ar' ? $this->operationStatus->name_ar : $this->operationStatus->name_en),
            'status_id' => $this->operationStatus == null ? null : $this->operationStatus->value,
            'status_class' => $this->operationStatus == null ? null : getOperationStatusString($this->operationStatus->value),
            'is_can_delete' => $this->operationStatus == null ? false : getOperationStatusCheckDelete($this->operationStatus->value),
            'is_can_edit' => $this->operationStatus == null ? false : getOperationStatusCheckEdit($this->operationStatus->value),
            'received' => $this->receivedable == null ? null : getUserAgencyResource($this->receivedable_type, $this->receivedable),

        ];
    }
}
