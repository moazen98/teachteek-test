<?php

namespace App\Http\Resources\Dashboard\Operation;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OperationCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'operations' => $this->collection->map(function ($operation) use ($request) {
                return (new OperationResource($operation))->toArray($request);
            })
        ];
    }
}
