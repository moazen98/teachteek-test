<?php

namespace App\Http\Resources\Dashboard\Currency;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CurrencyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'currencies' => $this->collection->map(function ($currency) use ($request) {
                return (new CurrencyResource($currency))->toArray($request);
            })
        ];
    }
}
