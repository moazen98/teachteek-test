<?php

namespace App\Http\Resources\Dashboard\User;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'name' => $this->first_name.' '.$this->last_name,
            'full_name' => $this->first_name . ' ' . $this->last_name,
            'job_title' => $this->job_title,
            'gender' => $this->gender,
            'birthdate' => $this->birthdate,
            'phone' => $this->phone,
            'email' => $this->email,
            'address' => $this->address,
            'note' => $this->note,
            'active' => $this->active,
            'active_string' => $this->active == 1 ? __('Active') : __('InActivate'),
            'register_date' => $this->created_at->format('y-m-d H:i'),
            'country' => $this->country == null ? null : (app()->getLocale() == 'ar' ? $this->country->name_ar : $this->country->name_en),
            'country_id' => $this->country == null ? null : $this->country->id,
            'city' => $this->city == null ? null : (app()->getLocale() == 'ar' ? $this->city->name_ar : $this->city->name_en),
            'city_id' => $this->city == null ? null : $this->city->id,
            'image_path' => $this->image_path,
        ];
    }
}
