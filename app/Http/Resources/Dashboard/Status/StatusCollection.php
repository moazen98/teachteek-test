<?php

namespace App\Http\Resources\Dashboard\Status;

use App\Http\Resources\Dashboard\Operation\StatusResrouce;
use Illuminate\Http\Resources\Json\ResourceCollection;

class StatusCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'status' => $this->collection->map(function ($status) use ($request) {
                return (new StatusResrouce($status))->toArray($request);
            })
        ];
    }
}
