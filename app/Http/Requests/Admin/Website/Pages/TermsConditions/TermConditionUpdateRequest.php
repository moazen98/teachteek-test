<?php

namespace App\Http\Requests\Admin\Website\Pages\TermsConditions;

use Illuminate\Foundation\Http\FormRequest;

class TermConditionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'description.required' => __('validation.required'),
        ];
    }
}
