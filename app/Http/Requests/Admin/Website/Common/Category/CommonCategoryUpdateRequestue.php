<?php

namespace App\Http\Requests\Admin\Website\Common\Category;

use Illuminate\Foundation\Http\FormRequest;

class CommonCategoryUpdateRequestue extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar' => 'required|unique:questions_categories,name_ar,' . $this->id,
            'name_en' => 'required|unique:questions_categories,name_en,' . $this->id,
        ];
    }


    public function messages()
    {
        return [
            'name_ar.required' => __('validation.required'),
            'name_ar.unique' => __('validation.unique'),
            'name_en.required' => __('validation.required'),
            'name_en.unique' => __('validation.unique'),
        ];
    }
}
