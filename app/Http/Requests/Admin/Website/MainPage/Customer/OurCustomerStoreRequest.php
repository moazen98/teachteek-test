<?php

namespace App\Http\Requests\Admin\Website\MainPage\Customer;

use Illuminate\Foundation\Http\FormRequest;

class OurCustomerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'title' => 'required',
            'customer_name' => 'required',
            'ratting' => 'required|numeric',
            'image.*' => 'mimes:jpg,jpeg,png'
        ];
    }

    public function messages()
    {
        return [
            'description.required' => __('validation.required'),
            'title.required' => __('validation.required'),
            'customer_name.required' => __('validation.required'),
            'ratting.required' => __('validation.required'),
            'ratting.numeric' => __('validation.numeric'),
            'image.*.mimes' => __('validation.mimes'),
        ];
    }
}
