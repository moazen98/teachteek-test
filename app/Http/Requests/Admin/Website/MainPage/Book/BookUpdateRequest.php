<?php

namespace App\Http\Requests\Admin\Website\MainPage\Book;

use Illuminate\Foundation\Http\FormRequest;

class BookUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'title' => 'required',
            'image.*' => 'mimes:jpg,jpeg,png'
        ];
    }

    public function messages()
    {
        return [
            'description.required' => __('validation.required'),
            'title.required' => __('validation.required'),
            'image.*.mimes' => __('validation.mimes'),
        ];
    }
}
