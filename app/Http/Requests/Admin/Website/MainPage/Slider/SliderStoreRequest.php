<?php

namespace App\Http\Requests\Admin\Website\MainPage\Slider;

use Illuminate\Foundation\Http\FormRequest;

class SliderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image.*' => 'mimes:jpg,jpeg,png',
            'image' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'image.required' => __('validation.required'),
            'image.*.mimes' => __('validation.mimes'),
        ];
    }
}
