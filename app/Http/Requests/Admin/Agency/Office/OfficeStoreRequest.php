<?php

namespace App\Http\Requests\Admin\Agency\Office;

use Illuminate\Foundation\Http\FormRequest;

class OfficeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:190',
            'email' => 'required|email|unique:offices|max:190',
            'phone' => 'required|numeric|digits:10|unique:offices',
            'full' => 'required',
            'password' => 'required|min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'required|min:6',
            'register_date' => 'nullable|date',
            'country' => 'required',
            'city' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __('validation.required'),
            'name.max' => __('validation.max_string'),
            'phone.required' => __('validation.required'),
            'phone.numeric' => trans('validation.numeric'),
            'phone.unique' => trans('validation.unique'),
            'phone.digits' => trans('validation.min_phone'),
            'full.required' => __('validation.required'),
            'email.required' => __('validation.required'),
            'email.max_string' => __('validation.max_string'),
            'email.email' => __('validation.email'),
            'email.unique' => __('validation.unique'),
            'password.required' => __('validation.email'),
            'password.min' => __('validation.min'),
            'password.same' => __('validation.same'),
            'confirm_password.min' => __('validation.min'),
            'confirm_password.required' => __('validation.required'),
            'register_date.date' => __('validation.date'),
            'country.required' => __('validation.required'),
            'city.required' => __('validation.required'),
        ];
    }
}
