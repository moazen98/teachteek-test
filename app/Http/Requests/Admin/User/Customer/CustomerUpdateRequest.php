<?php

namespace App\Http\Requests\Admin\User\Customer;

use Illuminate\Foundation\Http\FormRequest;

class CustomerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'required',
            'full' => 'required',
            'email' => 'required|email|unique:customers,email,' . $this->id,
            'password' => 'required_with:confirm_password|same:confirm_password',
            'image' => 'mimes:jpg,jpeg,png',
            'gender' => 'boolean',
            'birthdate' => 'date'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => __('validation.required'),
            'last_name.required' => __('validation.required'),
            'phone.required' => __('validation.required'),
            'full.required' => __('validation.required'),
            'email.required' => __('validation.required'),
            'email.email' => __('validation.email'),
            'email.unique' => __('validation.unique'),
            'password.min' => __('validation.min'),
            'password.same' => __('validation.same'),
            'confirm_password.min' => __('validation.min'),
            'image.mimes' => __('validation.file'),
            'gender.boolean' => __('validation.boolean'),
            'birthdate.date' => __('validation.date'),

        ];
    }
}
