<?php

namespace App\Http\Requests\Admin\User\Section;

use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SectionUpdateRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return RuleFactory::make([
            '%name%' => ['required',Rule::unique('section_translations', 'name')->ignore($this->id, 'section_id')],
            'image' => 'mimes:jpg,jpeg,png',
        ]);
    }

    public function attributes()
    {
        return RuleFactory::make([
            '%name%' => __('validation.attributes.name'),
        ]);
    }

    public function messages()
    {
        return [
            '*.name.required' => __('validation.required'),
            '*.name.unique' => __('validation.unique'),
            'active.boolean' => __('validation.boolean'),
            'image.mimes' => __('validation.mimes'),
        ];
    }
}
