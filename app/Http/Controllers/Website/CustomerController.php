<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\Customer\CustomerStoreRequest;
use App\Http\Requests\Admin\User\Customer\CustomerUpdateRequest;
use App\Http\Requests\Admin\User\Employee\EmployeeStoreRequest;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Http\Resources\Dashboard\User\CustomerResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    public function show()
    {
        $id = Auth::guard('customers')->user()->id;

        $customerData = app('servicesV1')->customerService->getUserById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('employee.index'), 'name' => __('Customers')],
            ['name' => __('Show employee')],
            ['name' => "#" . $customerData->id . ' - ' . $customerData->first_name . ' ' . $customerData->last_name . ' - ' . $customerData->created_at->format('m/d/Y')]
        ];

        $customer = (new CustomerResource($customerData))->toArray();

        return view('customer.user.show', compact('customer', 'breadcrumbs'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $id = Auth::guard('customers')->user()->id;
        $customerData = app('servicesV1')->customerService->getUserById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('employee.index'), 'name' => __('Customers')],
            ['name' => __('Edit employee')],
            ['name' => "#" . $customerData->id . ' - ' . $customerData->first_name . ' ' . $customerData->last_name . ' - ' . $customerData->created_at->format('m/d/Y')]
        ];

        $customer = (new CustomerResource($customerData))->toArray();

        $countries = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countries))->toArray();

        $citiesData = app('servicesV1')->locationService->getCitiesByCountryId($customerData->country->id);
        $cities = (new CityCollection($citiesData))->toArray();

        return view('customer.user.edit', compact('customer', 'countries', 'cities', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerUpdateRequest $request)
    {
        try {

            $id = Auth::guard('customers')->user()->id;

            $employees = app('servicesV1')->customerService->updateUser($request, $id);

            if ($employees) {
                return redirect()->back()->with('success', __('message.update'));
            } else {
                return redirect()->back()->with('failed', __('message.failed'));
            }

        } catch (\Exception $exception) {
            return redirect()->back()->with('success', __('message.failed'));
        }

    }

    public function store(CustomerStoreRequest $request)
    {

        try {
            $employees = app('servicesV1')->customerService->storeUser($request);

            if ($employees) {

                $employeesData = app('servicesV1')->customerService->getUserById($employees);
                Auth::guard('customers')->login($employeesData);

                return redirect()->route('customer.dashboard-ecommerce')
                    ->withSuccess('Signed in');

            } else {
                return redirect()->back()->with('failed', __('message.failed'));
            }

        } catch (\Exception $exception) {
            return redirect()->back()->with('success', __('message.failed'));
        }
    }

    public function registerCustomer()
    {


        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('employee.index'), 'name' => __('Customers')],
            ['name' => __('Add')]
        ];

        $countries = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countries))->toArray();


        return view('customer.user.create', compact('countries', 'breadcrumbs'));

    }
}
