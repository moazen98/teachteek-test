<?php

namespace App\Http\Controllers\Admin;

use App\Exports\CustomerExport;
use App\Exports\UserExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\Customer\CustomerStoreRequest;
use App\Http\Requests\Admin\User\Customer\CustomerUpdateRequest;
use App\Http\Requests\Admin\User\Employee\EmployeeStoreRequest;
use App\Http\Requests\Admin\User\Employee\EmployeeUpdateRequest;
use App\Http\Resources\Dashboard\Agency\User\UserResource;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Http\Resources\Dashboard\Role\RoleCollection;
use App\Http\Resources\Dashboard\Section\SectionCollection;
use App\Http\Resources\Dashboard\User\CustomerResource;
use App\Models\Role;
use App\Models\Section\Section;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CustomerController extends MainDashboardController
{
    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $request->page_counter;
        //create read update delete
        $this->middleware(['permission:read_customer'])->only('index');
        $this->middleware(['permission:create_customer'])->only('create');
        $this->middleware(['permission:update_customer'])->only('edit');
        $this->middleware(['permission:delete_customer'])->only('destroy');
        $this->middleware(['permission:read_customer'])->only('export');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Customers')]
        ];


        $customers = app('servicesV1')->customerService->getAllUsers($request)->paginate($this->dashboardPaginate);

        $customers->getCollection()->transform(function ($item) use ($request) {
            return (new CustomerResource($item))->toArray($request);
        });

        $countriesData = app('servicesV1')->locationService->getAllCountries($request);
        $countries = (new CountryCollection($countriesData))->toArray($request);


        return view('admin.user.customer.index', compact('customers', 'countries', 'breadcrumbs'));
    }


    public function fetchData(Request $request)
    {

        if ($request->ajax()) {

            $customers = app('servicesV1')->customerService->filterUsers($request)->paginate($this->dashboardPaginate);

            $customers->getCollection()->transform(function ($item) use ($request) {
                return (new CustomerResource($item))->toArray($request);
            });

            return view('admin.user.customer.include.pagination_data', compact('customers'))->render();
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('employee.index'), 'name' => __('Customers')],
            ['name' => __('Add')]
        ];

        $countries = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countries))->toArray();


        return view('admin.user.customer.create', compact('countries', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerStoreRequest $request)
    {

        try {
            $employees = app('servicesV1')->customerService->storeUser($request);

            if ($employees) {
                return redirect()->route('customer.index')->with('success', __('message.success'));
            } else {
                return redirect()->route('customer.index')->with('failed', __('message.failed'));
            }

        } catch (\Exception $exception) {
            return redirect()->route('customer.index')->with('success', __('message.failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $customerData = app('servicesV1')->customerService->getUserById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('employee.index'), 'name' => __('Customers')],
            ['name' => __('Show employee')],
            ['name' => "#" . $customerData->id . ' - ' . $customerData->first_name . ' ' . $customerData->last_name . ' - ' . $customerData->created_at->format('m/d/Y')]
        ];

        $customer = (new CustomerResource($customerData))->toArray();

        return view('admin.user.customer.show', compact('customer', 'breadcrumbs'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customerData = app('servicesV1')->customerService->getUserById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('employee.index'), 'name' => __('Customers')],
            ['name' => __('Edit employee')],
            ['name' => "#" . $customerData->id . ' - ' . $customerData->first_name . ' ' . $customerData->last_name . ' - ' . $customerData->created_at->format('m/d/Y')]
        ];

        $customer = (new CustomerResource($customerData))->toArray();

        $countries = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countries))->toArray();

        $citiesData = app('servicesV1')->locationService->getCitiesByCountryId($customerData->country->id);
        $cities = (new CityCollection($citiesData))->toArray();

        return view('admin.user.customer.edit', compact('customer', 'countries', 'cities', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerUpdateRequest $request, $id)
    {
        try {
            $employees = app('servicesV1')->customerService->updateUser($request, $id);

            if ($employees) {
                return redirect()->route('customer.index')->with('success', __('message.update'));
            } else {
                return redirect()->route('customer.index')->with('failed', __('message.failed'));
            }

        } catch (\Exception $exception) {
            return redirect()->route('customer.index')->with('success', __('message.failed'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $employee = app('servicesV1')->customerService->destroyUserById($id);

            if ($employee) {
                return redirect()->route('customer.index')->with('success', __('message.delete'));
            } else {
                return redirect()->route('customer.index')->with('failed', __('message.failed'));
            }

        } catch (\Exception $exception) {
            return redirect()->route('customer.index')->with('success', __('message.failed'));
        }
    }

    public function export()
    {

        return Excel::download(new CustomerExport(), 'customers.xlsx');
    }
}
