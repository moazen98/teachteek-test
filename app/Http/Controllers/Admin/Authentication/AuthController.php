<?php

namespace App\Http\Controllers\Admin\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Auth\LoginStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{



    public function login(LoginStoreRequest $request){

        $credentials = $request->only('email', 'password');


        if (Auth::attempt($credentials)) {

            $user = Auth::user();

            if ($user->active) {

                Auth::login($user, $request->get('remember'));


                return redirect()->route('admin.dashboard-ecommerce')
                    ->withSuccess('Signed in');
            } else {
                return redirect()->route('admin.get.login')
                    ->with('failed', __('message.login_active_failed'));
            }

        } else {
            return redirect()->route('admin.get.login')
                ->with('failed', __('message.login_username_password_wrong'));
        }

    }

    public function loginCustomer(LoginStoreRequest $request){

        $credentials = $request->only('email', 'password');



        if (Auth::guard('customers')->attempt($credentials)) {

            $user = Auth::guard('customers')->user();
            if ($user->active) {

                Auth::guard('customers')->login($user, $request->get('remember'));


                return redirect()->route('customer.dashboard-ecommerce')
                    ->withSuccess('Signed in');
            } else {
                return redirect()->route('customer.get.login')
                    ->with('failed', __('message.login_active_failed'));
            }

        } else {
            return redirect()->route('customer.get.login')
                ->with('failed', __('message.login_username_password_wrong'));
        }

    }

    public function getAdminLogin(){

        if (Auth::check()){

            return redirect()->route('admin.dashboard-ecommerce');

        }else{

            return view('admin.auth.login');
        }

    }


    public function getCustomerLogin(){

        if (Auth::check()){

            return redirect()->route('customer.dashboard-ecommerce');

        }else{
            return view('customer.auth.login');
        }

    }


    public function logout()
    {

        Auth::logout();
        return redirect()->route('admin.get.login');

    }

    public function logoutCustomer()
    {

        Auth::guard('customers')->logout();
        return redirect()->route('customer.get.login');

    }

    public function index(){

        if (Auth::check()){
            return redirect()->route('dashboard-ecommerce');
        }else{
            return redirect()->route('admin.get.login');
        }
    }
}
