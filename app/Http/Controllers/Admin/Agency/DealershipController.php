<?php

namespace App\Http\Controllers\Admin\Agency;

use App\Exports\BranchExport;
use App\Exports\DealershipExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Agency\Branch\BranchStoreRequest;
use App\Http\Requests\Admin\Agency\Branch\BranchUpdateRequest;
use App\Http\Requests\Admin\Agency\Dealership\DealershipStoreRequest;
use App\Http\Requests\Admin\Agency\Dealership\DealershipUpdateRequest;
use App\Http\Resources\Dashboard\Agency\Branch\BranchResource;
use App\Http\Resources\Dashboard\Agency\Dealer\DealerResource;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Models\Agency\Dealership;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class DealershipController extends MainDashboardController
{
    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_dealership'])->only('index');
        $this->middleware(['permission:create_dealership'])->only('create');
        $this->middleware(['permission:update_dealership'])->only('edit');
        $this->middleware(['permission:delete_dealership'])->only('destroy');
        $this->middleware(['permission:read_dealership'])->only('export');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Dealerships')]
        ];

        $dealers = app('servicesV1')->dealershipService->getAllDealer($request)->paginate($this->dashboardPaginate);

        $dealers->getCollection()->transform(function ($item) use ($request) {
            return (new DealerResource($item))->toArray($request);
        });

        $countries = app('servicesV1')->locationService->getAllCountries($request);
        $countries = (new CountryCollection($countries))->toArray($request);


        return view('admin.agency.dealer.index', compact('dealers', 'countries', 'breadcrumbs'));
    }


    public function fetch_data(Request $request)
    {
        if ($request->ajax()) {

            $dealers = app('servicesV1')->dealershipService->filterDealers($request)->paginate($this->dashboardPaginate);

            $dealers->getCollection()->transform(function ($item) use ($request) {
                return (new DealerResource($item))->toArray($request);
            });

            return view('admin.agency.dealer.include.pagination_data', compact('dealers'))->render();

        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('dealer.index'), 'name' => __('Dealerships')],
            ['name' => __('Add Dealership')]
        ];

        $countriesCollection = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countriesCollection))->toArray();

        return view('admin.agency.dealer.create', compact('countries', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(DealershipStoreRequest $request)
    {
//        try {
        $dealer = app('servicesV1')->dealershipService->storeDealer($request);

        return redirect()->route('dealer.index')->with('success', __('message.success'));

//        } catch (\Exception $exception) {
//            return redirect()->route('branch.index')->with('failed', __('message.failed'));
//
//        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $dealerData = app('servicesV1')->dealershipService->getDealerById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('dealer.index'), 'name' => __('Dealerships')],
            ['name' => __('Show Dealership')],
            ['name' => "#" . $dealerData->id . ' - ' . $dealerData->name . ' - ' . $dealerData->created_at->format('m/d/Y')]
        ];


        $dealer = (new DealerResource($dealerData))->toArray();

        return view('admin.agency.dealer.show', compact('dealer', 'breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dealerData = app('servicesV1')->dealershipService->getDealerById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('branch.index'), 'name' => __('Branches')],
            ['name' => __('Edit dealership')],
            ['name' => "#" . $dealerData->id . ' - ' . $dealerData->name . ' - ' . $dealerData->created_at->format('m/d/Y')]
        ];

        $dealer = (new DealerResource($dealerData))->toArray();
        $citiesData = app('servicesV1')->locationService->getCitiesByCountryId($dealerData->country->id);
        $cities = (new CityCollection($citiesData))->toArray();
        $countriesCollection = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countriesCollection))->toArray();

        return view('admin.agency.dealer.edit', compact('dealer', 'countries', 'cities', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(DealershipUpdateRequest $request, $id)
    {

        try {
            $dealer = app('servicesV1')->dealershipService->updateDealer($request, $id);

            return redirect()->route('dealer.index')->with('success', __('message.update'));

        } catch (\Exception $exception) {
            return redirect()->route('dealer.index')->with('failed', __('message.failed'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $dealer = app('servicesV1')->dealershipService->destroyDealerById($id);
            return redirect()->route('dealer.index')->with('success', __('message.delete'));
        } catch (\Exception $exception) {
            return redirect()->route('dealer.index')->with('failed', __('message.failed'));
        }

    }


    public function export()
    {
        try {

            ob_end_clean();

            return Excel::download(new DealershipExport(), __('Dealerships') . '.xlsx');
        } catch (\Exception $exception) {
            return redirect()->route('dealer.index')->with('failed', __('message.failed'));
        }
    }
}
