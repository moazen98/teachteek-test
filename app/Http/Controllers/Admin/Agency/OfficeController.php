<?php

namespace App\Http\Controllers\Admin\Agency;

use App\Enums\MediaFor;
use App\Exports\OfficeExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Agency\Office\OfficeStoreRequest;
use App\Http\Requests\Admin\Agency\Office\OfficeUpdateRequest;
use App\Http\Resources\Dashboard\Agency\Office\OfficeCollection;
use App\Http\Resources\Dashboard\Agency\Office\OfficeResource;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Location\City\CityResource;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Models\City;
use App\Models\Country;
use App\Models\Customer;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class OfficeController extends MainDashboardController
{


    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_office'])->only('index');
        $this->middleware(['permission:create_office'])->only('create');
        $this->middleware(['permission:update_office'])->only('edit');
        $this->middleware(['permission:delete_office'])->only('destroy');
        $this->middleware(['permission:read_office'])->only('export');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Offices')]
        ];

        $offices = app('servicesV1')->officeService->getAllOffices($request)->paginate($this->dashboardPaginate);

        $offices->getCollection()->transform(function ($item) use ($request) {
            return (new OfficeResource($item))->toArray($request);
        });

        $countries = app('servicesV1')->locationService->getAllCountries($request);
        $countries = (new CountryCollection($countries))->toArray($request);


        return view('admin.agency.office.index', compact('offices', 'countries', 'breadcrumbs'));
    }


    public function fetch_data(Request $request)
    {
        if ($request->ajax()) {

            $offices = app('servicesV1')->officeService->filterOffices($request)->paginate($this->dashboardPaginate);

            $offices->getCollection()->transform(function ($item) use ($request) {
                return (new OfficeResource($item))->toArray($request);
            });

            return view('admin.agency.office.include.pagination_data', compact('offices'))->render();

        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('office.index'), 'name' => __('Offices')],
            ['name' => __('Add Office')]
        ];

        $countriesCollection = Country::has('cities')->get();
        $countries = (new CountryCollection($countriesCollection))->toArray();

        return view('admin.agency.office.create', compact('countries', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(OfficeStoreRequest $request)
    {
        try {
            $office = app('servicesV1')->officeService->storeOffice($request);

            return redirect()->route('office.index')->with('success', __('message.success'));

        } catch (\Exception $exception) {
            return redirect()->route('office.index')->with('failed', __('message.failed'));

        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $officeData = app('servicesV1')->officeService->getOfficeById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('office.index'), 'name' => __('Offices')],
            ['name' => __('Show office')],
            ['name' => "#" . $officeData->id . ' - ' . $officeData->name . ' - ' . $officeData->created_at->format('m/d/Y')]
        ];


        $office = (new OfficeResource($officeData))->toArray();

        return view('admin.agency.office.show', compact('office', 'breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $officeData = app('servicesV1')->officeService->getOfficeById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('office.index'), 'name' => __('Offices')],
            ['name' => __('Edit office')],
            ['name' => "#" . $officeData->id . ' - ' . $officeData->name . ' - ' . $officeData->created_at->format('m/d/Y')]
        ];

        $office = (new OfficeResource($officeData))->toArray();
        $citiesData = app('servicesV1')->locationService->getCitiesByCountryId($officeData->country->id);
        $cities = (new CityCollection($citiesData))->toArray();
        $countriesCollection = Country::has('cities')->get();
        $countries = (new CountryCollection($countriesCollection))->toArray();

        return view('admin.agency.office.edit', compact('office', 'countries', 'cities', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(OfficeUpdateRequest $request, $id)
    {

        try {
            $office = app('servicesV1')->officeService->updateOffice($request, $id);

            return redirect()->route('office.index')->with('success', __('message.update'));

        } catch (\Exception $exception) {
            return redirect()->route('office.index')->with('failed', __('message.failed'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $office = app('servicesV1')->officeService->destroyOfficeById($id);
            return redirect()->route('office.index')->with('success', __('message.delete'));
        } catch (\Exception $exception) {
            return redirect()->route('office.index')->with('failed', __('message.failed'));
        }

    }


    public function export()
    {
        try {

            ob_end_clean();

            return Excel::download(new OfficeExport(), __('Offices') . '.xlsx');
        } catch (\Exception $exception) {
            return redirect()->route('office.index')->with('failed', __('message.failed'));
        }
    }
}
