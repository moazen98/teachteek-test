<?php

namespace App\Http\Controllers\Admin\Agency;

use App\Exports\BranchExport;
use App\Exports\OfficeExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Agency\Branch\BranchStoreRequest;
use App\Http\Requests\Admin\Agency\Branch\BranchUpdateRequest;
use App\Http\Requests\Admin\Agency\Office\OfficeStoreRequest;
use App\Http\Requests\Admin\Agency\Office\OfficeUpdateRequest;
use App\Http\Resources\Dashboard\Agency\Branch\BranchResource;
use App\Http\Resources\Dashboard\Agency\Office\OfficeResource;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Models\Country;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BranchesController extends MainDashboardController
{

    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $request->page_counter;
        //create read update delete
        $this->middleware(['permission:read_branch'])->only('index');
        $this->middleware(['permission:create_branch'])->only('create');
        $this->middleware(['permission:update_branch'])->only('edit');
        $this->middleware(['permission:delete_branch'])->only('destroy');
        $this->middleware(['permission:read_branch'])->only('export');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Branches')]
        ];

        $branches = app('servicesV1')->branchService->getAllBranches($request)->paginate($this->dashboardPaginate);

        $branches->getCollection()->transform(function ($item) use ($request) {
            return (new BranchResource($item))->toArray($request);
        });

        $countries = app('servicesV1')->locationService->getAllCountries($request);
        $countries = (new CountryCollection($countries))->toArray($request);


        return view('admin.agency.branch.index', compact('branches', 'countries', 'breadcrumbs'));
    }


    public function fetch_data(Request $request)
    {
        if ($request->ajax()) {

            $branches = app('servicesV1')->branchService->filterBranches($request)->paginate($this->dashboardPaginate);

            $branches->getCollection()->transform(function ($item) use ($request) {
                return (new BranchResource($item))->toArray($request);
            });

            return view('admin.agency.branch.include.pagination_data', compact('branches'))->render();

        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('office.index'), 'name' => __('Branches')],
            ['name' => __('Add Branch')]
        ];

        $countriesCollection = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countriesCollection))->toArray();

        return view('admin.agency.branch.create', compact('countries', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchStoreRequest $request)
    {
//        try {
            $branch = app('servicesV1')->branchService->storeBranch($request);

            return redirect()->route('branch.index')->with('success', __('message.success'));

//        } catch (\Exception $exception) {
//            return redirect()->route('branch.index')->with('failed', __('message.failed'));
//
//        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $branchData = app('servicesV1')->branchService->getBranchById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('branch.index'), 'name' => __('Branches')],
            ['name' => __('Show branch')],
            ['name' => "#" . $branchData->id . ' - ' . $branchData->name . ' - ' . $branchData->created_at->format('m/d/Y')]
        ];


        $branch = (new BranchResource($branchData))->toArray();

        return view('admin.agency.branch.show', compact('branch', 'breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branchData = app('servicesV1')->branchService->getBranchById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('branch.index'), 'name' => __('Branches')],
            ['name' => __('Edit branch')],
            ['name' => "#" . $branchData->id . ' - ' . $branchData->name . ' - ' . $branchData->created_at->format('m/d/Y')]
        ];

        $branch = (new BranchResource($branchData))->toArray();
        $citiesData = app('servicesV1')->locationService->getCitiesByCountryId($branchData->country->id);
        $cities = (new CityCollection($citiesData))->toArray();
        $countriesCollection = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countriesCollection))->toArray();

        return view('admin.agency.branch.edit', compact('branch', 'countries', 'cities', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BranchUpdateRequest $request, $id)
    {

        try {
            $branch = app('servicesV1')->branchService->updateBranch($request, $id);

            return redirect()->route('branch.index')->with('success', __('message.update'));

        } catch (\Exception $exception) {
            return redirect()->route('branch.index')->with('failed', __('message.failed'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $branch = app('servicesV1')->branchService->destroyBranchById($id);
            return redirect()->route('branch.index')->with('success', __('message.delete'));
        } catch (\Exception $exception) {
            return redirect()->route('branch.index')->with('failed', __('message.failed'));
        }

    }


    public function export()
    {
        try {

            ob_end_clean();

            return Excel::download(new BranchExport(), __('Branches') . '.xlsx');
        } catch (\Exception $exception) {
            return redirect()->route('branch.index')->with('failed', __('message.failed'));
        }
    }
}
