<?php

namespace App\Http\Controllers\Admin\User;

use App\Exports\UserExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Requests\Admin\User\Employee\EmployeeStoreRequest;
use App\Http\Requests\Admin\User\Employee\EmployeeUpdateRequest;
use App\Http\Resources\Dashboard\Agency\User\UserResource;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Http\Resources\Dashboard\Role\RoleCollection;
use App\Http\Resources\Dashboard\Section\SectionCollection;
use App\Models\Role;
use App\Models\Section\Section;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends MainDashboardController
{

    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $request->page_counter;
        //create read update delete
        $this->middleware(['permission:read_employee'])->only('index');
        $this->middleware(['permission:create_employee'])->only('create');
        $this->middleware(['permission:update_employee'])->only('edit');
        $this->middleware(['permission:delete_employee'])->only('destroy');
        $this->middleware(['permission:read_employee'])->only('export');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Employees')]
        ];


        $employees = app('servicesV1')->userService->getAllUsers($request)->paginate($this->dashboardPaginate);

        $employees->getCollection()->transform(function ($item) use ($request) {
            return (new UserResource($item))->toArray($request);
        });

        $countriesData = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countriesData))->toArray($request);


        return view('admin.user.employee.index', compact('employees', 'countries', 'breadcrumbs'));
    }


    public function fetchData(Request $request)
    {

        if ($request->ajax()) {

            $employees = app('servicesV1')->userService->filterUsers($request)->paginate($this->dashboardPaginate);

            $employees->getCollection()->transform(function ($item) use ($request) {
                return (new UserResource($item))->toArray($request);
            });

            return view('admin.user.employee.include.pagination_data', compact('employees'))->render();
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('employee.index'), 'name' => __('Employees')],
            ['name' => __('Add')]
        ];

        $rolesCollection = Role::all();
        $roles = (new RoleCollection($rolesCollection))->toArray(null);

        $countries = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countries))->toArray();

        $sectionsCollection = Section::where('active', 1)->get();
        $sections = (new SectionCollection($sectionsCollection))->toArray();

        return view('admin.user.employee.create', compact('roles', 'countries', 'sections', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeStoreRequest $request)
    {

        try {
            $employees = app('servicesV1')->userService->storeUser($request);

            if ($employees) {
                return redirect()->route('employee.index')->with('success', __('message.success'));
            } else {
                return redirect()->route('employee.index')->with('failed', __('message.failed'));
            }

        } catch (\Exception $exception) {
            return redirect()->route('employee.index')->with('success', __('message.failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $employeeData = app('servicesV1')->userService->getUserById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('employee.index'), 'name' => __('Employees')],
            ['name' => __('Show employee')],
            ['name' => "#" . $employeeData->id . ' - ' . $employeeData->first_name . ' ' . $employeeData->last_name. ' - ' . $employeeData->created_at->format('m/d/Y')]
        ];

        $employee = (new UserResource($employeeData))->toArray();

        return view('admin.user.employee.show', compact('employee', 'breadcrumbs'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employeeData = app('servicesV1')->userService->getUserById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('employee.index'), 'name' => __('Employees')],
            ['name' => __('Edit employee')],
            ['name' => "#" . $employeeData->id . ' - ' . $employeeData->first_name . ' ' . $employeeData->last_name. ' - ' . $employeeData->created_at->format('m/d/Y')]
        ];

        $employee = (new UserResource($employeeData))->toArray();

        $rolesCollection = Role::all();
        $roles = (new RoleCollection($rolesCollection))->toArray();

        $countries = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countries))->toArray();

        $citiesData = app('servicesV1')->locationService->getCitiesByCountryId($employeeData->country->id);
        $cities = (new CityCollection($citiesData))->toArray();
        $sectionsCollection = Section::where('active', 1)->get();
        $sections = (new SectionCollection($sectionsCollection))->toArray();

        return view('admin.user.employee.edit', compact('employee', 'roles', 'countries', 'sections', 'cities', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeUpdateRequest $request, $id)
    {
        try {
            $employees = app('servicesV1')->userService->updateUser($request,$id);

            if ($employees) {
                return redirect()->route('employee.index')->with('success', __('message.update'));
            } else {
                return redirect()->route('employee.index')->with('failed', __('message.failed'));
            }

        } catch (\Exception $exception) {
            return redirect()->route('employee.index')->with('success', __('message.failed'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $employee = app('servicesV1')->userService->destroyUserById($id);

            if ($employee) {
                return redirect()->route('employee.index')->with('success', __('message.delete'));
            } else {
                return redirect()->route('employee.index')->with('failed', __('message.failed'));
            }

        } catch (\Exception $exception) {
            return redirect()->route('employee.index')->with('success', __('message.failed'));
        }
    }

    public function export()
    {

        return Excel::download(new UserExport(), 'employees.xlsx');
    }
}
