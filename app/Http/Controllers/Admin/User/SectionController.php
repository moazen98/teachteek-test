<?php

namespace App\Http\Controllers\Admin\User;

use App\Exports\SectionExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\Section\SectionStoreRequest;
use App\Http\Requests\Admin\User\Section\SectionUpdateRequest;
use App\Models\Section\Section;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class SectionController extends Controller
{


    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_section'])->only('index');
        $this->middleware(['permission:create_section'])->only('create');
        $this->middleware(['permission:update_section'])->only('edit');
        $this->middleware(['permission:delete_section'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['name' => __('Sections')]
        ];

        $sections = Section::when($request->search, function ($q) use ($request) {

            return $q->where('name_ar', 'like', '%' . $request->search . '%')
                ->orWhere('name_en', 'like', '%' . $request->search . '%')
                ->orWhere('description', 'like', '%' . $request->search . '%');

        })->latest()->paginate(10);

        return view('admin.user.section.index', compact('sections', 'breadcrumbs'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['link' => "/admin/users/section/index", 'name' => __('Sections')],
            ['name' => __('Add')]
        ];

        return view('admin.user.section.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SectionStoreRequest $request)
    {
        $section = new Section();

        $requestData = $request->all();

        if ($request->image) {

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('storage/uploads/sections/' . $request->image->hashName()));

            $requestData['image_url'] = $request->image->hashName();
        }else {
            $section->image_url = 'school.jpg';
        }

//        foreach (config('translatable.locales') as $local){
//            $section->{"name:".$local} = $request->
//        }
        /**
         * "title" => [
         *  "en" => "",
         *
         * '
         */
        $section->create($requestData);

        return redirect()->route('section.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['link' => "/admin/users/section/index", 'name' => __('Sections')],
            ['name' => __('Show')]
        ];

        $section = Section::findOrFail($id);

        $employees = $section->employees;



        return view('admin.user.section.show', compact('section', 'employees','breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['link' => "/admin/users/section/index", 'name' => __('Sections')],
            ['name' => __('Edit')]
        ];

        $section = Section::findOrFail($id);


        return view('admin.user.section.edit', compact('section', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SectionUpdateRequest $request, $id)
    {

        $section = Section::findOrFail($id);

        $requestData = $request->except('active');

        if ($request->image) {

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('storage/uploads/sections/' . $request->image->hashName()));

            $requestData['image_url'] = $request->image->hashName();
        }

        if ($request->has('active')) {
            $section->active = 1;
        } else {
            $section->active = 0;
        }

        $section->update($requestData);

        return redirect()->route('section.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = Section::find($id);
        $section->delete();

        return redirect()->route('section.index')->with('success', __('message.delete'));
    }

    public function export(){
        return Excel::download(new SectionExport(), 'sections.xlsx');
    }
}
