<?php

namespace App\Http\Controllers\Admin\Website\MainPage\Book;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\MainPage\Book\BookStoreRequest;
use App\Http\Requests\Admin\Website\MainPage\Book\BookUpdateRequest;
use App\Models\Book;
use App\Models\BookGallary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class BookGallaryController extends Controller
{

    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_books_gallary'])->only('index');
        $this->middleware(['permission:create_books_gallary'])->only('create');
        $this->middleware(['permission:update_books_gallary'])->only('edit');
        $this->middleware(['permission:delete_books_gallary'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['name' => __('Books')]
        ];

        $books = BookGallary::paginate(10);

        return view('admin.website.mainPage.book.index', compact('books', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['link' => "/admin/website-pages/main-page/books/index", 'name' => __('Website Books')],
            ['name' => __('Add Book')]
        ];

        $books = Book::where('active',1)->latest()->get();

        return view('admin.website.mainPage.book.create', compact('books','breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookStoreRequest $request)
    {
        $book = new BookGallary();
        $book->description = $request->description;
        $book->title = $request->title;
        $book->user()->associate(Auth::user());
        $bookRelated = Book::find($request->book_id);
        $book->book()->associate($bookRelated);

        if ($request->has('image')) {
            $files = upload_files(Config::get('doc.website_book'), 'book', 'jpeg|jpg|png', $request);

            $book->image_url = Config::get('doc.website_book') . $files[0]['real_path'];
        } else {
            $book->image_url = 'images/book/book.jpg';
        }

        $book->save();

        return redirect()->route('website.main.book.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = BookGallary::findOrFail($id);

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['link' => "/admin/website-pages/main-page/books/index", 'name' => __('Website Books')],
            ['name' => __('Edit Book')],
            ['name' => "# " . $book->title ],
        ];


        $books = Book::where('active',1)->latest()->get();

        return view('admin.website.mainPage.book.edit', compact('book','books', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookUpdateRequest $request, $id)
    {
        $book = BookGallary::findOrFail($id);
        $book->description = $request->description;
        $book->title = $request->title;
        $book->user()->associate(Auth::user());
        $bookRelated = Book::find($request->book_id);
        $book->book()->associate($bookRelated);
        if ($request->has('image')) {
            $files = upload_files(Config::get('doc.website_book'), 'book', 'jpeg|jpg|png', $request);

            $book->image_url = Config::get('doc.website_book') . $files[0]['real_path'];



        }

        $book->save();

        return redirect()->route('website.main.book.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = BookGallary::findOrFail($id);
        $partner->delete();

        return redirect()->route('website.main.book.index')->with('success', __('message.delete'));
    }
}
