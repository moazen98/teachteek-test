<?php

namespace App\Http\Controllers\Admin\Website\MainPage\Slider;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\MainPage\Slider\SliderStoreRequest;
use App\Http\Requests\Admin\Website\MainPage\Slider\SliderUpdateRequest;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class SliderController extends Controller
{


    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_slider'])->only('index');
        $this->middleware(['permission:create_slider'])->only('create');
        $this->middleware(['permission:update_slider'])->only('edit');
        $this->middleware(['permission:delete_slider'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['name' => __('Slider Page')]
        ];


        $images = Slider::paginate(10);

        return view('admin.website.mainPage.slider.index', compact('images', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['link' => "/admin/website-pages/main-page/slider/index", 'name' => __('Slider Page')],
            ['name' => __('Add Image')]
        ];

        return view('admin.website.mainPage.slider.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderStoreRequest $request)
    {
        $slider = new Slider();

        if ($request->has('image')) {
            $files = upload_files(Config::get('doc.website_slider'), 'slider', 'jpeg|jpg|png', $request);

            $slider->image_url = Config::get('doc.website_slider') . $files[0]['real_path'];

            $slider->user()->associate(Auth::user());

            $slider->save();
        }

        return redirect()->route('website.main.slider.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['link' => "/admin/website-pages/main-page/slider/index", 'name' => __('Slider Page')],
            ['name' => __('Edit Image')]
        ];

        $slider = Slider::find($id);


        return view('admin.website.mainPage.slider.edit', compact('slider', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('image')) {

            $slider = Slider::find($id);

            $files = upload_files(Config::get('doc.website_slider'), 'slider', 'jpeg|jpg|png', $request);

            $slider->image_url = Config::get('doc.website_slider') . $files[0]['real_path'];

            $slider->user()->associate(Auth::user());

            $slider->save();
        }


        return redirect()->route('website.main.slider.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::find($id);
        $slider->delete();

        return redirect()->route('website.main.slider.index')->with('success', __('message.delete'));
    }
}
