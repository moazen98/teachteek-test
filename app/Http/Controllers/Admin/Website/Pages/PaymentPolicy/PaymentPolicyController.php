<?php

namespace App\Http\Controllers\Admin\Website\Pages\PaymentPolicy;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\Pages\PaymentPolicy\PaymentPolicyStoreRequest;
use App\Http\Requests\Admin\Website\Pages\PaymentPolicy\PaymentPolicyUpdateRequest;
use App\Models\PaymentPolicy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentPolicyController extends Controller
{
    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_payment_policy'])->only('index');
        $this->middleware(['permission:create_payment_policy'])->only('create');
        $this->middleware(['permission:update_payment_policy'])->only('edit');
        $this->middleware(['permission:delete_payment_policy'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Payment Privacy')]
        ];

        $privacy = PaymentPolicy::first();

        return view('admin.website.pages.payment.index', compact('privacy', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Create Payment Privacy')]
        ];

        return view('admin.website.pages.payment.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentPolicyStoreRequest $request)
    {
        $policy = new PaymentPolicy();
        $policy->description = $request->description;
        $policy->user()->associate(Auth::user());

        $policy->save();

        return redirect()->route('website.page.payment.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Edit Payment Privacy')]
        ];

        $privacy = PaymentPolicy::find($id);

        return view('admin.website.pages.payment.edit', compact('breadcrumbs', 'privacy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PaymentPolicyUpdateRequest $request, $id)
    {
        $policy = PaymentPolicy::find($id);
        $policy->description = $request->description;
        $policy->user()->associate(Auth::user());
        $policy->save();

        return redirect()->route('website.page.payment.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $policy = PaymentPolicy::find($id);
        $policy->delete();

        return redirect()->route('website.page.payment.index')->with('success', __('message.delete'));
    }
}
