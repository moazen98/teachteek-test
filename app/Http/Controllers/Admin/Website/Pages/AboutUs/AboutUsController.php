<?php

namespace App\Http\Controllers\Admin\Website\Pages\AboutUs;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\Pages\AboutUs\AboutUsStoreRequest;
use App\Http\Requests\Admin\Website\Pages\AboutUs\AboutUsUpdateRequest;
use App\Models\AboutUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AboutUsController extends Controller
{
    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_about_us'])->only('index');
        $this->middleware(['permission:create_about_us'])->only('create');
        $this->middleware(['permission:update_about_us'])->only('edit');
        $this->middleware(['permission:delete_about_us'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('About Us')]
        ];

        $aboutUs = AboutUs::first();


        return view('admin.website.mainPage.aboutUs.index', compact('aboutUs', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Create About Us')]
        ];

        return view('admin.website.mainPage.aboutUs.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AboutUsStoreRequest $request)
    {
        $aboutUs = new AboutUs();
        $aboutUs->description = $request->description;
        $aboutUs->user()->associate(Auth::user());

        $aboutUs->save();

        return redirect()->route('website.page.about.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Edit About Us')]
        ];

        $aboutUs = AboutUs::findOrFail($id);

        return view('admin.website.mainPage.aboutUs.edit', compact('breadcrumbs', 'aboutUs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AboutUsUpdateRequest $request, $id)
    {
        $aboutUs = AboutUs::findOrFail($id);
        $aboutUs->description = $request->description;
        $aboutUs->user()->associate(Auth::user());
        $aboutUs->save();

        return redirect()->route('website.page.about.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aboutUs = AboutUs::findOrFail($id);
        $aboutUs->delete();

        return redirect()->route('website.page.about.index')->with('success', __('message.delete'));
    }
}
