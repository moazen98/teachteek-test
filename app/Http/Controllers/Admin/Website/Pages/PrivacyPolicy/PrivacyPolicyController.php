<?php

namespace App\Http\Controllers\Admin\Website\Pages\PrivacyPolicy;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\Pages\PrivacyPolicy\PrivacyPolicyStoreRequest;
use App\Http\Requests\Admin\Website\Pages\PrivacyPolicy\PrivacyPolicyUpdateRequest;
use App\Models\Policy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PrivacyPolicyController extends Controller
{
    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_privacy_policy'])->only('index');
        $this->middleware(['permission:create_payment_policy'])->only('create');
        $this->middleware(['permission:update_privacy_policy'])->only('edit');
        $this->middleware(['permission:delete_privacy_policy'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Privacy and Policy')]
        ];

        $privacy = Policy::first();

        return view('admin.website.pages.privacy.index', compact('privacy', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Create Privacy and Policy')]
        ];

        return view('admin.website.pages.privacy.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrivacyPolicyStoreRequest $request)
    {
        $policy = new Policy();
        $policy->description = $request->description;
        $policy->user()->associate(Auth::user());

        $policy->save();

        return redirect()->route('website.page.privacy.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Edit Privacy and Policy')]
        ];

        $privacy = Policy::find($id);

        return view('admin.website.pages.privacy.edit', compact('breadcrumbs', 'privacy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrivacyPolicyUpdateRequest $request, $id)
    {
        $policy = Policy::find($id);
        $policy->description = $request->description;
        $policy->user()->associate(Auth::user());
        $policy->save();

        return redirect()->route('website.page.privacy.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $policy = Policy::find($id);
        $policy->delete();

        return redirect()->route('website.page.privacy.index')->with('success', __('message.delete'));
    }
}
