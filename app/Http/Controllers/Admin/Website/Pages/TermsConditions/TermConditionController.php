<?php

namespace App\Http\Controllers\Admin\Website\Pages\TermsConditions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\Pages\TermsConditions\TermConditionStoreRequest;
use App\Http\Requests\Admin\Website\Pages\TermsConditions\TermConditionUpdateRequest;
use App\Models\TermCondition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TermConditionController extends Controller
{
    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_terms_conditions'])->only('index');
        $this->middleware(['permission:create_terms_conditions'])->only('create');
        $this->middleware(['permission:update_terms_conditions'])->only('edit');
        $this->middleware(['permission:delete_terms_conditions'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Terms and Conditions')]
        ];

        $privacy = TermCondition::first();

        return view('admin.website.pages.term.index', compact('privacy', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Create Terms and Conditions')]
        ];

        return view('admin.website.pages.term.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TermConditionStoreRequest $request)
    {
        $policy = new TermCondition();
        $policy->description = $request->description;
        $policy->user()->associate(Auth::user());

        $policy->save();

        return redirect()->route('website.page.term.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Edit Terms and Conditions')]
        ];

        $privacy = TermCondition::find($id);

        return view('admin.website.pages.term.edit', compact('breadcrumbs', 'privacy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(TermConditionUpdateRequest $request, $id)
    {
        $policy = TermCondition::find($id);
        $policy->description = $request->description;
        $policy->user()->associate(Auth::user());
        $policy->save();

        return redirect()->route('website.page.term.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $policy = TermCondition::find($id);
        $policy->delete();

        return redirect()->route('website.page.term.index')->with('success', __('message.delete'));
    }
}
