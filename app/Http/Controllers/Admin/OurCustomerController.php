<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\MainPage\Customer\OurCustomerStoreRequest;
use App\Http\Requests\Admin\Website\MainPage\Customer\OurCustomerUpdateRequest;
use App\Models\Customer;
use App\Models\OurCustomer;
use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class OurCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['name' => __('Our Customers Page')]
        ];

        $customers = OurCustomer::paginate(10);

        return view('admin.website.mainPage.customer.index', compact('customers', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['link' => "/admin/website-pages/main-page/our-customers/index", 'name' => __('Our Customers')],
            ['name' => __('Add Customer')]
        ];

        return view('admin.website.mainPage.customer.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OurCustomerStoreRequest $request)
    {
        $customer = new OurCustomer();
        $customer->description = $request->description;
        $customer->title = $request->title;
        $customer->ratting = $request->ratting;
        $customer->customer_name = $request->customer_name;
        $customer->user()->associate(Auth::user());


        if ($request->has('image')) {
            $files = upload_files(Config::get('doc.website_partner'), 'partner', 'jpeg|jpg|png', $request);

            $customer->image_url = Config::get('doc.website_partner') . $files[0]['real_path'];

        } else {
            $customer->image_url = 'images/author/author.png';
        }

        $customer->save();

        return redirect()->route('website.main.customer.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['link' => "/admin/website-pages/main-page/our-customers/index", 'name' => __('Our Customers')],
            ['name' => __('Edit Customer')]
        ];

        $customer = OurCustomer::findOrFail($id);

        return view('admin.website.mainPage.customer.edit', compact('customer', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OurCustomerUpdateRequest $request, $id)
    {
        $customer = OurCustomer::findOrFail($id);
        $customer->description = $request->description;
        $customer->title = $request->title;
        $customer->ratting = $request->ratting;
        $customer->customer_name = $request->customer_name;
        $customer->user()->associate(Auth::user());


        if ($request->has('image')) {
            $files = upload_files(Config::get('doc.website_partner'), 'partner', 'jpeg|jpg|png', $request);

            $customer->image_url = Config::get('doc.website_partner') . $files[0]['real_path'];

        }

        $customer->save();

        return redirect()->route('website.main.customer.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = OurCustomer::findOrFail($id);
        $customer->delete();

        return redirect()->route('website.main.customer.index')->with('success', __('message.delete'));
    }
}
