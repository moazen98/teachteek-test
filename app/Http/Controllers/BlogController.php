<?php

namespace App\Http\Controllers;

use App\Http\Resources\BlogCollection;
use App\Http\Resources\BlogResouce;
use App\Http\Resources\GifCollection;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Giphy;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{


    public function create()
    {

        $id = Auth::guard('customers')->user()->id;
        $customerData = app('servicesV1')->customerService->getUserById($id);


        $breadcrumbs = [
            ['link' => route('customer.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Blogs')],
            ['name' => __('Add blog')],
            ['name' => "#" . $customerData->id . ' - ' . $customerData->first_name . ' ' . $customerData->last_name . ' - ' . $customerData->created_at->format('m/d/Y')]
        ];
        $giphys = Giphy::search('cat', 10);
        $gifs = (new GifCollection($giphys->data))->toArray();
        return view('customer.blog.create', compact('gifs', 'breadcrumbs'));

    }

    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => route('customer.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Blogs')],
        ];

        $blogsData = app('servicesV1')->blogService->getAllBlogs();
        $blogs = (new BlogCollection($blogsData))->toArray();


        return view('customer.blog.index', compact('breadcrumbs', 'blogs'));

    }

    public function show($id, Request $request)
    {

        $breadcrumbs = [
            ['link' => route('customer.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Blogs')],
        ];

        $blog = app('servicesV1')->blogService->findBlog($id);
        $blog = (new BlogResouce($blog))->toArray();
        return view('customer.blog.show', compact('breadcrumbs', 'blog'));

    }

    public function searchGif(Request $request)
    {

        $giphys = Giphy::search($request->get('query'), 10);


        $gifs = (new GifCollection($giphys->data))->toArray();

        return view('customer.blog.pagination_data', compact('gifs'))->render();

    }


    public function store(Request $request)
    {

        try {

            $data = app('servicesV1')->blogService->storeBlog($request);
            if ($data) {
                return redirect()->route('website.blog.index')->with('success', __('message.success'));
            } else {
                return redirect()->route('website.blog.index')->with('failed', __('message.failed'));
            }

        } catch (\Exception $exception) {
            return redirect()->route('website.blog.index')->with('failed', __('message.failed'));
        }

    }

    public function searchText(Request $request)
    {

        $blogsData = app('servicesV1')->blogService->filterBlogs($request)->get();

        $blogs = (new BlogCollection($blogsData))->toArray();

        return view('customer.blog.pagination_data_index', compact( 'blogs'))->render();

    }
}
