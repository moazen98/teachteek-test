<?php

namespace App\Models\Section;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use SoftDeletes;

    protected $table = 'sections';
    protected $fillable = ['description','active','image_url'];
    public $translatedAttributes = ['name'];
    protected $appends = ['image_path'];

    public function employees(){
        return $this->hasMany(User::class,'section_id','id');
    }

    public function getImagePathAttribute()
    {
        return asset('storage/uploads/sections/' . $this->image_url);

    }//end of get image path
}
