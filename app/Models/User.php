<?php

namespace App\Models;

use App\Models\Section\Section;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
//    use HasApiTokens;
    use LaratrustUserTrait;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use SoftDeletes;

    protected $table = 'users';


    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'job_title',
        'phone',
        'email',
        'password',
        'address',
        'note',
        'active',
        'image_url',
        'section_id',
        'country_id',
        'city_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'image_path',
    ];


    //TODO: here we use set attribute for password field
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }


    public function section()
    {
        return $this->belongsTo(Section::class, 'section_id', 'id')->withTrashed();
    }


    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }


    public function getImagePathAttribute()
    {
        return asset(Config::get('custom_settings.image_employee_default_url') . $this->image_url);

    }//end of get image path

    public function setImage_urlAttributes(Request $request)
    {
        if ($request->has('image')) {

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path(Config::get('custom_settings.image_employee_default_url') . $request->image->hashName()));

            $this->attributes['image_url'] = $request->image->hashName();
        } else {

            $this->attributes['image_url'] = $request->gender == 1 ? Config::get('custom_settings.image_employee_male_default') : Config::get('custom_settings.image_employee_female_default');

        }
    }
}
