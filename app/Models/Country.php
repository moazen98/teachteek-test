<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $table = 'countries';

    protected $fillable = [
        'name_ar',
        'name_fr',
        'name_en',
        'code'
    ];

    public function cities()
    {
        return $this->hasMany('App\Models\City');
    }

}
