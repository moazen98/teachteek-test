<?php

namespace App\Models\Agency;

use App\Models\City;
use App\Models\Country;
use App\Models\File;
use App\Models\MediaExtension;
use App\Models\Operation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class Office extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'offices';
    protected $fillable = ['name', 'email', 'phone', 'international_code', 'password', 'is_active', 'register_date', 'note', 'last_login_date', 'image_url','country_id','city_id'];

    protected $appends = [
        'image_path',
    ];


    //TODO: here we use set attribute for password field
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }


    public function getImagePathAttribute()
    {
        return asset(Config::get('custom_settings.image_office_default_url') . $this->image_url);

    }//end of get image path


    public function setting()
    {
        return $this->morphOne(AgencySetting::class, 'settingable');
    }

    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    public function operations()
    {
        return $this->morphMany(Operation::class, 'operationable');
    }

    public function receivedOperations()
    {
        return $this->morphMany(Operation::class, 'receivedable');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }


    public function storeFiles(Request $request)
    {


        if ($request->has('files')) {
            $filesRequest = $request->all()['files'];
            foreach ($filesRequest as $key => $value) {

                $files = upload_single_files(Config::get('custom_settings.image_agency'),  $this->id, 'png|jpg|jpeg|pdf|xls', $value);

                $data = $files[0];

                $this->files()->updateOrCreate([
                    'url' => $data['file_name'],
                    'real_name' => $data['real_name'],
                    'size' => $data['size'],
                    'extension_id' => MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id,
                ]);

            }
        }
    }

    public function setImage_urlAttributes(Request $request)
    {
        if ($request->has('image')) {

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path(Config::get('custom_settings.image_agency') . $this->id .'/'. $request->image->hashName()));

            $this->attributes['image_url'] = $request->image->hashName();
        } else {

            $this->attributes['image_url'] = Config::get('custom_settings.image_office_default');

        }
    }
}
