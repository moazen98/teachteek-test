<?php

namespace App\Models\Agency;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

Relation::morphMap([
    'office' => Office::class,
    'dealership' => Dealership::class,
    'branch' => Branch::class,
]);

class AgencySetting extends Model
{
    use HasFactory;
    protected $table = 'agency_setting';
    protected $fillable = ['is_turkey','is_dollar','is_euro','is_gold','is_change_id','is_show_name','is_depended'];


    public function settingable(){
        return $this->morphTo('settingable');
    }

}
