<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Laravel\Sanctum\HasApiTokens;

class Customer extends Authenticatable
{
    use HasFactory;
    use HasApiTokens;
    use SoftDeletes;

    protected $table = 'customers';


    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'international_code',
        'email',
        'password',
        'address',
        'gender',
        'birthdate',
        'note',
        'active',
        'image_url',
        'country_id',
        'city_id',
        'verified',
        'fcm_token',
        'provider_name',
        'provider_id',
        'username'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'image_path',
    ];


    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }


    public function blogs(){
        return $this->hasMany(Blog::class,'customer_id','id');
    }



    //TODO: here we use set attribute for password field
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }



    public function getImagePathAttribute()
    {
        return asset(Config::get('custom_settings.image_employee_default_url') . $this->image_url);

    }//end of get image path

    public function setImage_urlAttributes(Request $request)
    {
        if ($request->has('image')) {

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path(Config::get('custom_settings.image_employee_default_url') . $request->image->hashName()));

            $this->attributes['image_url'] = $request->image->hashName();
        } else {

            $this->attributes['image_url'] = $request->gender == 1 ? Config::get('custom_settings.image_employee_male_default') : Config::get('custom_settings.image_employee_female_default');

        }
    }

}
