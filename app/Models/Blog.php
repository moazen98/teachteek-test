<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    protected $table = 'blogs';
    protected $fillable = ['description','customer_id'];


    public function customer(){
        return $this->belongsTo(Customer::class,'customer_id','id');
    }

    public function images(){
        return $this->hasMany(BlogImage::class,'blog_id','id');
    }
}
