<?php

namespace App\Models;

use App\Models\Agency\Branch;
use App\Models\Agency\Dealership;
use App\Models\Agency\Office;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Config;

Relation::morphMap([
    'office' => Office::class,
    'dealership' => Dealership::class,
    'branch' => Branch::class,
]);

class File extends Model
{
    use HasFactory;
    protected $table = 'files';
    protected $fillable = ['url','extension_id','size','real_name','fileable_type','fileable_id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'file_path',
    ];


    public function fileable(){
        return $this->morphTo('fileable');
    }

    public function extension(){
        return $this->belongsTo(MediaExtension::class,'extension_id','id');
    }

    public function getFilePathAttribute()
    {
        return asset(Config::get('custom_settings.image_agency') . $this->fileable_id .'/'. $this->url);

    }
}
