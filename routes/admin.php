<?php

use App\Http\Controllers\Admin\Area\AreaController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\User\UserController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\DashboardController;
use \App\Http\Controllers\LanguageController;
use \App\Http\Controllers\Admin\Authentication\AuthController;
use \App\Http\Controllers\Admin\User\RoleController;
use \App\Http\Controllers\Admin\User\SectionController;

Route::post('login', [AuthController::class, 'login'])->name('admin.login');
Route::get('login', [AuthController::class, 'getAdminLogin'])->name('admin.get.login');
Route::get('/', [AuthController::class, 'index'])->name('admin.get.index');
Route::group(['prefix' => 'area'], function () {
    Route::post('get-city', [AreaController::class, 'index'])->name('area.cities');
});


Route::group(['middleware' => ['auth']], function () {

    Route::get('/dashboard', [DashboardController::class, 'dashboardEcommerce'])->name('admin.dashboard-ecommerce');
    Route::get('/logout', [AuthController::class, 'logout'])->name('admin.logout');


    Route::group(['prefix' => 'users'], function () {

        Route::group(['prefix' => 'employee'], function () {


            Route::get('index', [UserController::class, 'index'])->name('employee.index');
            Route::get('/pagination/fetch-data', [UserController::class, 'fetchData'])->name('employee.fetchData');
            Route::get('create', [UserController::class, 'create'])->name('employee.create');
            Route::get('show/{id}', [UserController::class, 'show'])->name('employee.show');
            Route::get('edit/{id}', [UserController::class, 'edit'])->name('employee.edit');
            Route::post('update/{id}', [UserController::class, 'update'])->name('employee.update');
            Route::post('store', [UserController::class, 'store'])->name('employee.store');
            Route::delete('delete/{id}', [UserController::class, 'destroy'])->name('employee.destroy');
            Route::get('profile/{id}', [UserController::class, 'profile'])->name('employee.profile');
            Route::get('export', [UserController::class, 'export'])->name('employee.export');

        });

        Route::group(['prefix' => 'role'], function () {

            Route::get('index', [RoleController::class, 'index'])->name('role.index');
            Route::get('create', [RoleController::class, 'create'])->name('role.create');
            Route::get('show/{id}', [RoleController::class, 'show'])->name('role.show');
            Route::get('edit/{id}', [RoleController::class, 'edit'])->name('role.edit');
            Route::post('update/{id}', [RoleController::class, 'update'])->name('role.update');
            Route::post('store', [RoleController::class, 'store'])->name('role.store');
            Route::delete('delete/{id}', [RoleController::class, 'destroy'])->name('role.destroy');
            Route::get('export', [RoleController::class, 'export'])->name('role.export');

        });


        Route::group(['prefix' => 'section'], function () {

            Route::get('index', [SectionController::class, 'index'])->name('section.index');
            Route::get('create', [SectionController::class, 'create'])->name('section.create');
            Route::get('show/{id}', [SectionController::class, 'show'])->name('section.show');
            Route::get('edit/{id}', [SectionController::class, 'edit'])->name('section.edit');
            Route::post('update/{id}', [SectionController::class, 'update'])->name('section.update');
            Route::post('store', [SectionController::class, 'store'])->name('section.store');
            Route::delete('delete/{id}', [SectionController::class, 'destroy'])->name('section.destroy');
            Route::get('export', [SectionController::class, 'export'])->name('section.export');


        });


    });

    Route::group(['prefix' => 'customers'], function () {


        Route::get('index', [CustomerController::class, 'index'])->name('customer.index');
        Route::get('/pagination/fetch-data', [CustomerController::class, 'fetchData'])->name('customer.fetchData');
        Route::get('create', [CustomerController::class, 'create'])->name('customer.create');
        Route::get('show/{id}', [CustomerController::class, 'show'])->name('customer.show');
        Route::get('edit/{id}', [CustomerController::class, 'edit'])->name('customer.edit');
        Route::post('update/{id}', [CustomerController::class, 'update'])->name('customer.update');
        Route::post('store', [CustomerController::class, 'store'])->name('customer.store');
        Route::delete('delete/{id}', [CustomerController::class, 'destroy'])->name('customer.destroy');
        Route::get('profile/{id}', [CustomerController::class, 'profile'])->name('customer.profile');
        Route::get('export', [CustomerController::class, 'export'])->name('customer.export');

    });


});


Route::get('lang/{locale}', [LanguageController::class, 'swap'])->name('lang.swap');
