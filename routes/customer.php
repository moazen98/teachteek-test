<?php

use App\Http\Controllers\Admin\Area\AreaController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\Website\CustomerController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\DashboardController;
use \App\Http\Controllers\LanguageController;
use \App\Http\Controllers\Admin\Authentication\AuthController;

Route::post('login', [AuthController::class, 'loginCustomer'])->name('customer.login');
Route::get('login', [AuthController::class, 'getCustomerLogin'])->name('customer.get.login');
Route::get('/', [AuthController::class, 'index'])->name('customer.get.index');
Route::get('/register-account', [CustomerController::class, 'registerCustomer'])->name('customer.get.register');
Route::post('register-account-store', [CustomerController::class, 'store'])->name('website.customer.store');

Route::group(['prefix' => 'area'], function () {
    Route::post('get-city', [AreaController::class, 'index'])->name('area.cities');
});

Route::group(['middleware' => ['auth:customers']], function () {

    Route::get('/dashboard', [DashboardController::class, 'dashboardEcommerceCustomer'])->name('customer.dashboard-ecommerce');
    Route::get('/logout', [AuthController::class, 'logoutCustomer'])->name('customer.logout');




    Route::group(['prefix' => 'profile'], function () {


        Route::get('show', [CustomerController::class, 'show'])->name('website.customer.show');
        Route::get('edit', [CustomerController::class, 'edit'])->name('website.customer.edit');
        Route::post('update', [CustomerController::class, 'update'])->name('website.customer.update');

    });

    Route::group(['prefix' => 'blog'], function () {

        Route::get('create', [BlogController::class, 'create'])->name('website.blog.index');

    });

    Route::group(['prefix' => 'blogs'], function () {

        Route::get('create', [BlogController::class, 'create'])->name('website.blog.index');
        Route::get('index', [BlogController::class, 'index'])->name('website.blog.index');
        Route::get('show/{id}', [BlogController::class, 'show'])->name('website.blog.show');
        Route::post('store', [BlogController::class, 'store'])->name('website.blog.store');
        Route::get('search-gif', [BlogController::class, 'searchGif'])->name('website.blog.searchGif');
        Route::get('search-text', [BlogController::class, 'searchText'])->name('website.blog.searchText');

    });


});


Route::get('lang/{locale}', [LanguageController::class, 'swap'])->name('lang.swap');
