<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\AuthenticationController;
use \App\Http\Controllers\Api\SliderController;
use \App\Http\Controllers\Api\MobileServicesController;
use \App\Http\Controllers\Api\AdvertController;
use \App\Http\Controllers\Api\CustomerController;
use \App\Http\Controllers\Api\PrivacyController;
use \App\Http\Controllers\Api\AddressController;
use \App\Http\Controllers\Api\ContactUsController;
use \App\Http\Controllers\Api\OrderController;
use \App\Http\Controllers\Api\MobileSectionController;
use \App\Http\Controllers\Api\AboutUsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware(['SanctumAuth'])->group(function () {

});
