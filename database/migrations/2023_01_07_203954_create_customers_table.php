<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('password');
            $table->string('image_url')->nullable();
            $table->string('job_title')->nullable();
            $table->string('address')->nullable();
            $table->text('note')->nullable();
            $table->boolean('gender')->nullable();
            $table->boolean('active')->default(true);
            $table->date('birthdate')->nullable();
            $table->timestamp('email_verified_at')->nullable();

            $table->bigInteger('country_id')->unsigned()->nullable();

            $table->bigInteger('city_id')->unsigned()->nullable();

            $table->softDeletes();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
