<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Section\Section;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name' => 'Mohamad',
            'last_name' => 'Al Moazen',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => '123456', // 123456
            'phone' => '0999999999', // 123456
            'remember_token' => Str::random(10),
            'gender' => 1,
            'active' => 1,
            'section_id' => Section::first()->id,
            'country_id' => Country::first() == null ? null : Country::first()->id,
            'city_id' => Country::first() == null ? null : Country::first()->cities()->first()->id,
            'birthdate' => now(),
            'image_url' => Config::get('custom_settings.image_employee_male_default'),
        ]);

        $user->attachRole('super_admin');
    }
}
