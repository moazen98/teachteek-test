<?php

namespace Database\Seeders;

use App\Models\Section\Section;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $section = Section::create([
            'description' => Str::random(10),
            'active' => 1,
        ]);

        foreach (config('translatable.locales') as $locale){
            $attr[$locale]['name'] = Str::random(10);
            $section->update($attr);
        }


    }
}
