<?php

namespace Database\Seeders;

use App\Enums\MediaExtension;
use Illuminate\Database\Seeder;

class MediaExtensionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $medias = MediaExtension::getValues();
        foreach ($medias as $media){
            $model = new \App\Models\MediaExtension();
            $model->extension = $media;
            $model->save();
        }
    }
}
