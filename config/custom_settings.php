<?php


return [
    'dashboard_paginate' => 15,
    'default_office_image' => 'office-default.png',
    'media_office' => 'storage'.'/'.'office'.'/'.'media'.'/',
    'media_branch' => 'storage'.'/'.'branch'.'/'.'media'.'/',
    'image_office' => 'storage'.'/'.'office'.'/'.'media'.'/',
    'image_office_default' => 'office-default.png',
    'image_branch_default' => 'branch-default.png',
    'image_dealership_default' => 'dealership-default.png',
    'image_employee_male_default' => 'employee_male_default.jpg',
    'image_employee_female_default' => 'employee_female_default.jpg',
    'image_office_default_url' => 'dashboard/images/agency/office/',
    'image_branch_default_url' => 'dashboard/images/agency/branch/',
    'image_dealership_default_url' => 'dashboard/images/agency/dealership/',
    'image_employee_default_url' => 'dashboard/images/employee/',
    'image_agency' => 'storage'.'/'.'agency'.'/'.'files'.'/',
];
