<?php

return [
    'role_structure' => [
        'super_admin' => [
            'employee' => 'c,r,u,d',
            'customer' => 'c,r,u,d',
            'role' => 'c,r,u,d',
            'section' => 'c,r,u,d',
            'blogs' => 'c,r,u,d',
        ],
        'admin' => []
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
