@foreach ($customers as $index => $customer)
    <tr>
        <td>{{$customer['id']}}</td>
        <td>
            <div class="avatar"><img style="width:30px;height: 30px" src="{{$customer['image_path']}}">
            </div> {{$customer['full_name']}}</td>
        <td>{{$customer['email']}}</td>


        <td>@if($customer['active'] == 1)
                <span
                    class="badge rounded-pill badge-light-success">{{__('Active')}}</span>
            @else
                <span
                    class="badge rounded-pill badge-light-danger">{{__('InActive')}}</span>
            @endif</td>


        <td>
            <div class="dropdown">
                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    @permission('update_customer')
                    <a class="dropdown-item" href="{{route('customer.edit',$customer['id'])}}">
                        <i data-feather="edit-2" class="me-50"></i>
                        <span>{{__('Edit')}}</span>
                    </a>
                    @endpermission


                    @permission('read_customer')
                    <a class="dropdown-item" href="{{route('customer.show',$customer['id'])}}">
                        <i data-feather="eye" class="me-50"></i>
                        <span>{{__('Show')}}</span>
                    </a>
                    @endpermission


                    @permission('delete_customer')
                        <form method="post"
                              action="{{route('customer.destroy',$customer['id'])}}">
                            @csrf
                            @method('DELETE')
                            <button class="btn delete delete-style dropdown-item"><i data-feather="trash" class="me-50"></i>
                                <span>{{__('Delete')}}</span></button>
                        </form>
                    @endpermission

                </div>
            </div>
        </td>


    </tr>
@endforeach
{{--<tr>--}}
<td colspan="3">
    {!! $customers->appends(request()->query())->links("pagination::bootstrap-4") !!}
</td>
{{--</tr>--}}

<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>

