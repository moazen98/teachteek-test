<script>
    $(document).ready(function () {


        function fetch_data(page, sort_by, column_name, query, country, city, page_counter) {
            $.ajax({
                url: "/admin/customers/pagination/fetch-data?page=" + page + "&sortby=" + sort_by  + "&query=" + query + "&country=" + country  + "&city=" + city  + "&page_counter=" + page_counter,
                success: function (data) {
                    $('tbody').html('');
                    $('tbody').html(data);
                }
            })
        }

        $(document).on('keyup', '#search', function () {

            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var country = $('#COUNTRY_CATEGORY').val();
            var city = $('#CITY').val();
            var page = $('#hidden_page').val();
            fetch_data(page, sort_type, column_name, query, country, city, page_counter);
        });

        $(document).on('change', '#COUNTRY_CATEGORY', function () {

            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var country = $('#COUNTRY_CATEGORY').val();
            var city = $('#CITY').val();
            var page = $('#hidden_page').val();
            fetch_data(page, sort_type, column_name, query, country, city, page_counter);
        });

        $(document).on('change', '#hidden_sort_type', function () {

            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var country = $('#COUNTRY_CATEGORY').val();
            var city = $('#CITY').val();
            var page = $('#hidden_page').val();
            fetch_data(page, sort_type, column_name, query, country, city, page_counter);
        });

        $(document).on('change', '#CITY', function () {

            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var country = $('#COUNTRY_CATEGORY').val();
            var city = $('#CITY').val();
            var page = $('#hidden_page').val();
            fetch_data(page, sort_type, column_name, query, country, city, page_counter);
        });


        $(document).on('click', '.pagination a', function (event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            $('#hidden_page').val(page);
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var query = $('#search').val();
            var page_counter = $('#page_counter').val();
            var country = $('#COUNTRY_CATEGORY').val();
            var city = $('#CITY').val();

            $('li').removeClass('active');
            $(this).parent().addClass('active');
            fetch_data(page, sort_type, column_name, query, country, city, page_counter);
        });
    });
</script>
