<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput-jquery.js"></script>
<script src="{{asset('build/js/intlTelInput.js')}}"></script>

<script>
    $(document).ready(function () {
        $('#phone').keyup(function () {
            var numbers = $(this).val();
            $(this).val(numbers.replace(/\D/, ''));
        });
    });
</script>



<script>
    $(document).ready(function () {
        $(".dropdown-toggle").dropdown();
    });
</script>

<script>
    $(document).ready(function () {

        //delete
        $('.delete').click(function (e) {

            var that = $(this)

            e.preventDefault();

            var n = new swal({
                title: '{{__('Are you sure you want to delete this record?')}}',
                text: "{{__('If you delete this, it will be gone forever.')}}",
                icon: "warning",
                // buttons: true,
                showCancelButton: true,
                showConfirmButton: true,
                dangerMode: true,
                type: "danger",
                closeOnConfirm: false,
                confirmButtonClass: "btn-danger",
            }).then((willDelete) => {
                if (willDelete['isConfirmed'] == true) {
                    that.closest('form').submit();
                } else {
                    // n.close();
                }
            });

        });//end of delete
    });//end of ready

</script>

<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>


<script>
    $(document).ready(function () {
        $(".mul-select2").select2({
            placeholder: "إختر الصلاحيات", //placeholder
            tags: true,
            tokenSeparators: ['/', ',', ';', " "],
        });
    })
</script>




<script>
    $(document).ready(function () {

        $(document).on('change', '#COUNTRY_CATEGORY', function () {

            var country_id = $("#COUNTRY_CATEGORY").val();
            $.ajax({
                url: "{{ route('area.cities') }}",
                type: "post",
                mode: "abort",
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    country_id: country_id,
                },
                success: function (data) {
                    var locate = "{!! config('app.locale') !!}";
                    if (locate == 'ar') {
                        jQuery('select[name="city"]').empty();
                        jQuery.each(data, function (key, value) {
                            $('select[name="city"]').append('<option value="' + data[key].id + '">' + data[key].name_ar + '</option>')
                        });
                    } else {
                        jQuery('select[name="city"]').empty();
                        jQuery.each(data, function (key, value) {
                            $('select[name="city"]').append('<option value="' + data[key].id + '">' + data[key].name_en + '</option>')
                        });
                    }

                }, error: function (data) {
                    console.log('error');
                }

            })
        })


    })

</script>





<script>
    $(document).ready(function () {

        $("#regForm").validate({
            rules: {
                first_name: "required",
                last_name: "required",
                section: "required",
                email: "required",
                role: "required",
            },
            messages: {
                first_name: {
                    required: "{{trans('validation.required')}}",
                },
                last_name: {
                    required: "{{trans('validation.required')}}",
                },
                section: {
                    required: "{{trans('validation.required')}}",
                },
                email: {
                    required: "{{trans('validation.required')}}",
                },
                role: {
                    required: "{{trans('validation.required')}}",
                },
            }
        });
    });
</script>

<script>
    $('document').ready(function () {
        $("#uploadInputImage").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploadInputPreview').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
</script>
