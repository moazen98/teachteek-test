@extends('layouts.contentLayoutMaster')

@section('title', __('Show Employee'))

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <!-- vendor css files part2 -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/katex.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/monokai-sublime.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.bubble.css')) }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Inconsolata&family=Roboto+Slab&family=Slabo+27px&family=Sofia&family=Ubuntu+Mono&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">

@endsection

@section('page-style')
    {{-- Page Css files --}}


    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection

@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-body">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left card-title">{{ __('General Info') }}</h3>
                            <div class="float-right" @if(app()->getLocale() == 'en')style="margin-left: 600px"
                                 @else style="margin-right: 600px" @endif>

                                <div class="row">
                                    @permission('update_employee')
                                    <div class="col-5">
                                        <a type="button" class="btn btn-primary"
                                           href="{{ route('employee.edit',$employee['id']) }}"><i class="fa fa-edit"></i></a>
                                    </div>
                                    @endpermission

                                    @permission('delete_employee')
                                    <div class="col-5">
                                        <form method="post" id="myForm"
                                              action="{{route('employee.destroy',$employee['id'])}}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="button" class="btn btn-danger delete" title="{{__('Delete')}}"
                                                  ><i class="fa fa-trash"></i></button>
                                        </form>
                                    </div>
                                    @endpermission

                                </div>

                            </div>


                        </div>
                        <div class="user-avatar-section">
                            <div class="d-flex align-items-center flex-column">
                                <img
                                    class="img-fluid rounded mt-3 mb-2"
                                    src="{{$employee['image_path']}}"
                                    height="110"
                                    width="110"
                                    alt="User avatar"
                                />
                                <div class="user-info text-center">
                                    <h4>{{$employee['full_name']}}</h4>
                                    <span class="badge bg-light-info">{{$employee['job_title']}}</span>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="card-body">


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('First Name') }}:</h4>
                                    <p> {{ $employee['first_name'] }} </p>
                                </div>
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Last Name') }}:</h4>
                                    <p> {{ $employee['last_name'] }} </p>
                                </div>
                            </div>


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{__('Employee Section')}}:</h4>
                                    <p> {{$employee['section_string']}}</p>
                                </div>
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Job Title') }}:</h4>
                                    <p> {{ $employee['job_title'] }} </p>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Phone Number') }}:</h4>
                                    <p> {{ $employee['phone'] }} </p>
                                </div>

                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Email') }}:</h4>
                                    <p> {{ $employee['email'] }} </p>
                                </div>
                            </div>

                            {{-- Status & Classification --}}
                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Country') }}:</h4>
                                    <p> {{ $employee['country'] }} </p>
                                </div>

                                <div class="col-md-6 col-12">
                                    <h4>{{ __('City') }}:</h4>
                                    <p> {{ $employee['city'] }} </p>
                                </div>


                            </div>


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Address') }}:</h4>
                                    <p> {{ $employee['address'] }} </p>
                                </div>


                            </div>

                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Status') }}:</h4>
                                    {{--                                    <p> {{ $employee->active ? __('locale.doctor.active') : __('locale.doctor.not_active') }} </p>--}}
                                    @if($employee['active'])
                                        <span
                                            class="badge rounded-pill badge-light-success">{{__('Active')}}</span>
                                    @else
                                        <span
                                            class="badge rounded-pill badge-light-danger">{{__('InActive')}}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-12">
                                    <h4>{{ __('Notes') }}:</h4>
                                    <p> {{ $employee['note'] }} </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    @php
                        $models = ['employee','role','section','blogs'];
                        $maps = ['create', 'read', 'update', 'delete'];
                    @endphp

                    <div class="content-wrapper container-xxl p-0">
                        <div class="content-body">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="col-12">
                                                <h4 class="mt-2 pt-50">{{__('Role Permissions')}} :@if(app()->getLocale() == 'ar') {{$employee['role_object']->name_ar}} @else {{$employee['role_object']->name}} @endif</h4>
                                                <!-- Permission table -->
                                                <div class="table-responsive">
                                                    <table class="table table-flush-spacing">
                                                        <tbody>
                                                        @foreach ($models as $index=>$model)

                                                            @php
                                                                $res = str_replace( array('_'), ' ', $model);
                                                            @endphp
                                                            <tr>
                                                                <td class="text-nowrap fw-bolder">{{__($res)}}</td>
                                                                <td>
                                                                    @foreach ($maps as $map)
                                                                        <div class="d-flex">
                                                                            <div class="form-check me-3 me-lg-5">
                                                                                <input class="form-check-input"
                                                                                       type="checkbox"
                                                                                       name="permissions[]"
                                                                                       {{ $employee['role_object']->hasPermission($map . '_' . $model) ? 'checked' : '' }}
                                                                                       id="userManagementRead"
                                                                                       value="{{ $map . '_' . $model }}"
                                                                                       readonly
                                                                                       disabled
                                                                                />
                                                                                <label class="form-check-label"
                                                                                       for="userManagementRead">
                                                                                    {{__($map)}} </label>
                                                                            </div>
                                                                            @endforeach
                                                                        </div>
                                                                </td>
                                                            </tr>

                                                        @endforeach



                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- Permission table -->
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>

@endsection
@section('page-script')
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>


    @include('admin.partials.scripts')

@endsection
