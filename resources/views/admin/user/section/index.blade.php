@extends('layouts/contentLayoutMaster')

@section('title', __('Show Sections'))

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection


@include('admin.user.section.include.styles')


@section('content')


    @include('admin.partials.alert')

    <form action="{{route('section.index')}}" method="GET">

        <div class="row">
            <div class="form-group col-4" style="margin-bottom: 10px">
                <input type="text" class="form-control" name="search" id="search"
                       placeholder="{{__('Search By Name , Description')}}"
                       value="{{ request()->input('search') }}">
                <span class="text-danger">@error('search'){{ $message }} @enderror</span>
            </div>

            <div class="form-group col-4">
                <button type="submit" class="btn btn-primary" @if(app()->getLocale() == 'en')style="margin-left: 10px" @else style="margin-right: 10px" @endif>{{__('Search')}}</button>
            </div>

        </div>
    </form>


    <!-- Column Search -->
    <section id="ajax-datatable">

        <div class="row">


            <div class="col-12">


                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">{{__('Sections Info')}}</h4>

                        <div class="row">
                        @permission('create_employee')
                            <div class="col-6">
                        <a href="{{route('section.create')}}" @if(app()->getLocale() == 'en')style="margin-right: 60px!important;white-space: nowrap;max-width: 100%;" @else style="margin-left: 60px!important;white-space: nowrap;max-width: 100%;" @endif>
                            <button type="button"
                                    class="btn btn-primary">{{ __('Add Section') }}
                                <i class="fa fa-plus"></i>
                            </button>
                        </a>
                            </div>
                        @endpermission

                        <div class="col-6">
                            <a href="{{route('section.export')}}">
                                <button type="button"
                                        class="btn btn-success">{{ __('Export') }}
                                    <i class="fa fa-file-excel-o"></i>
                                </button>
                            </a>
                        </div>

                        </div>

                    </div>

                    <div class="card-datatable overflow-auto">
                        <table class="datatables-ajax table table-responsive ">
                            <thead>
                            <tr>
                                <th>{{__('ID')}}</th>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Description')}}</th>
                                <th>{{__('Employees Number')}}</th>
                                <th>{{__('Status')}}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>

                            <tbody>

                            @foreach ($sections as $index => $section)
                                <tr>
                                    <td>{{++$index}}</td>
                                    <td>{{$section->name}}</td>
                                    <td>{{$section->description}}</td>
                                    <td>{{count($section->employees)}}</td>
                                    <td>@if($section->active == 1)
                                            <span
                                                class="badge rounded-pill badge-light-success">{{__('Active')}}</span>
                                        @else
                                            <span
                                                class="badge rounded-pill badge-light-danger">{{__('InActive')}}</span>
                                        @endif</td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0"
                                                    data-bs-toggle="dropdown">
                                                <i data-feather="more-vertical"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                @permission('update_section')
                                                <a class="dropdown-item"
                                                   href="{{route('section.edit',$section-> id)}}">
                                                    <i data-feather="edit-2" class="me-50"></i>
                                                    <span>{{__('Edit')}}</span>
                                                </a>
                                                @endpermission


                                                @permission('read_section')
                                                <a class="dropdown-item"
                                                   href="{{route('section.show',$section-> id)}}">
                                                    <i data-feather="eye" class="me-50"></i>
                                                    <span>{{__('Show')}}</span>
                                                </a>
                                                @endpermission


                                                @permission('delete_section')
                                                <div class="col-4">
                                                    <form method="post" id="myForm"
                                                          action="{{route('section.destroy',$section-> id)}}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn delete dropdown-item"><i data-feather="trash"
                                                                                      class="me-50"></i>
                                                            <span>{{__('Delete')}}</span></button>
                                                    </form>
                                                </div>
                                                @endpermission

                                            </div>
                                        </div>
                                    </td>


                                </tr>
                            @endforeach

                            </tbody>

                            </thead>
                        </table>
                    </div>

                    {!! $sections->appends(request()->query())->links("pagination::bootstrap-4") !!}


                    <input type="hidden" name="hidden_page" id="hidden_page" value="1"/>
                    <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="id"/>
                    <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc"/>

                </div>
            </div>
        </div>
    </section>
    <!--/ Column Search -->



@endsection


@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    {{--        <script src="{{ asset(mix('js/scripts/tables/table-datatables-advanced.js')) }}"></script>--}}
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>

@endsection


@include('admin.user.section.include.scripts')


