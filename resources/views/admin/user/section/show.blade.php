@extends('layouts/contentLayoutMaster')

@section('title', __('Show Employee'))

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <!-- vendor css files part2 -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/katex.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/monokai-sublime.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.bubble.css')) }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Inconsolata&family=Roboto+Slab&family=Slabo+27px&family=Sofia&family=Ubuntu+Mono&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">

@endsection

@section('page-style')
    {{-- Page Css files --}}
    <style>
        .m-img {
            width: inherit;
        }

        .card .card-title {
            font-size: 2rem;
        }

    </style>

    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-body">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left card-title">{{ __('General Info') }}</h3>
                            <div class="float-right" @if(app()->getLocale() == 'en')style="margin-left: 600px"
                                 @else style="margin-right: 600px" @endif>

                                <div class="row">
                                    @permission('update_section')
                                    <div class="col-5">
                                        <a type="button" class="btn btn-warning"
                                           href="{{ route('section.edit',$section->id) }}">{{ __('Edit') }}</a>
                                    </div>
                                    @endpermission

                                    @permission('delete_section')
                                    <div class="col-5">
                                        <form method="post" id="myForm"
                                              action="{{route('section.destroy',$section-> id)}}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="button" class="btn btn-danger delete" title="{{__('Delete')}}"
                                                    style="padding-right: 10px">{{__('Delete')}}</button>
                                        </form>
                                    </div>
                                    @endpermission

                                </div>

                                {{--                                <a type="button" class="btn btn-danger delete"--}}
                                {{--                                   href="{{ route('employee.destroy',$employee->id) }}">{{ __('Delete') }}</a>--}}

                            </div>


                        </div>
                        <div class="user-avatar-section">
                            <div class="d-flex align-items-center flex-column">
                                <img
                                    class="img-fluid rounded mt-3 mb-2"
                                    src="{{asset($section->image_path)}}"
                                    height="110"
                                    width="110"
                                    alt="User avatar"
                                />
                                <div class="user-info text-center">
                                    @if(app()->getLocale() == 'ar')
                                        <h4>{{$section->name_ar}}</h4>
                                    @else
                                        <h4>{{$section->name_en}}</h4>
                                    @endif
                                    @if($section->active == 1)
                                        <span
                                            class="badge rounded-pill badge-light-success">{{__('Active')}}</span>
                                    @else
                                        <span
                                            class="badge rounded-pill badge-light-danger">{{__('InActive')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="card-body">


                            <div class="row mb-2">
                                @foreach (config('translatable.locales') as $locale)
                                    <div class="col-md-6 col-12">
                                        <h4>{{ __('Name') }} {{__($locale)}}:</h4>
                                        <p> {{$section->translate($locale)->name}} </p>
                                    </div>


                                @endforeach
                            </div>


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Status') }}:</h4>
                                    {{--                                    <p> {{ $employee->active ? __('locale.doctor.active') : __('locale.doctor.not_active') }} </p>--}}
                                    @if($section->active)
                                        <span
                                            class="badge rounded-pill badge-light-success">{{__('Active')}}</span>
                                    @else
                                        <span
                                            class="badge rounded-pill badge-light-danger">{{__('InActive')}}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-12">
                                    <h4>{{ __('Description') }}:</h4>
                                    <p> {{ $section->description }} </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="content-body">
            <div class="content-wrapper container-xxl p-0">
                <div class="content-body">
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="col-12">
                                        <h4 class="mt-2 pt-50">{{__('Section Employees')}}</h4>
                                        <!-- Permission table -->
                                        <div class="table-responsive">
                                            <table class="table table-flush-spacing">
                                                <tr>
                                                    <th>{{__('ID')}}</th>
                                                    <th>{{__('Full Name')}}</th>
                                                    <th>{{__('Email')}}</th>
                                                    <th>{{__('Role')}}</th>
                                                    <th>{{__('Status')}}</th>
                                                    <th>{{ __('Action') }}</th>
                                                </tr>
                                                <tbody>
                                                @foreach ($employees as $index => $employee)
                                                    <tr>
                                                        <td>{{++$index}}</td>
                                                        <td>{{$employee->first_name.' '.$employee->last_name}}</td>
                                                        <td>{{$employee->email}}</td>
                                                        @if(!is_null($employee->roles()->first()))
                                                            @if(app()->getLocale() == 'en')
                                                                <td><i class="fa fa-database text-danger"></i> {{ $employee->roles()->first()->name}}</td>
                                                            @else
                                                                <td><i class="fa fa-database text-danger"></i> {{$employee->roles()->first()->name_ar}}</td>
                                                            @endif
                                                        @else
                                                            <td>---</td>
                                                        @endif
                                                        <td>@if($employee->active == 1)
                                                                <span
                                                                    class="badge rounded-pill badge-light-success">{{__('Active')}}</span>
                                                            @else
                                                                <span
                                                                    class="badge rounded-pill badge-light-danger">{{__('InActive')}}</span>
                                                            @endif</td>


                                                        <td>
                                                            <div class="dropdown">
                                                                <button type="button"
                                                                        class="btn btn-sm dropdown-toggle hide-arrow py-0"
                                                                        data-bs-toggle="dropdown">
                                                                    <i data-feather="more-vertical"></i>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-end">
                                                                    @permission('update_employee')
                                                                    <a class="dropdown-item"
                                                                       href="{{route('employee.edit',$employee-> id)}}">
                                                                        {{--                                                                        <i data-feather="edit-2"--}}
                                                                        {{--                                                                           class="me-50"></i>--}}
                                                                        <span>{{__('Edit')}}</span>
                                                                    </a>
                                                                    @endpermission


                                                                    @permission('read_employee')
                                                                    <a class="dropdown-item"
                                                                       href="{{route('employee.show',$employee-> id)}}">
                                                                        {{--                                                                        <i data-feather="eye" class="me-50"></i>--}}
                                                                        <span>{{__('Show')}}</span>
                                                                    </a>
                                                                    @endpermission


                                                                    @permission('delete_employee')
                                                                    <div class="col-4">
                                                                        <form method="post" id="myForm"
                                                                              action="{{route('employee.destroy',$employee-> id)}}">
                                                                            @csrf
                                                                            @method('DELETE')
                                                                            <button class="btn delete">
                                                                                {{--                                                                                <i--}}
                                                                                {{--                                                                                    data-feather="trash"--}}
                                                                                {{--                                                                                    class="me-50"></i>--}}
                                                                                <span>{{__('Delete')}}</span>
                                                                            </button>
                                                                        </form>
                                                                    </div>
                                                                    @endpermission

                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- Permission table -->
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>

@endsection
@section('page-script')
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>

@endsection

@include('admin.user.section.include.scripts')
