@extends('layouts/contentLayoutMaster')

@section('title', __('Add Section'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">

@endsection


@section('content')
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

    <div class="row">


        <section id="basic-horizontal-layouts">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{__('Add Section to the System')}}</h4>
                    </div>
                    <div class="alert-body">
                        <!-- Vertical Wizard -->
                        <div class="card-body">
                            <form action="{{ route('section.store') }}" method="post" enctype="multipart/form-data"
                                  id="regForm">
                                @csrf

                                <div class="mb-1">
                                    <div class="row col-12 mb-2">
                                        @foreach (config('translatable.locales') as $locale)
                                            <div class="col-6">
                                                <label class="form-label"
                                                       for="basic-addon-name">{{ __('Name') }} {{__($locale)}} *</label>
                                                <input
                                                    type="text"
                                                    id="basic-addon-name"
                                                    class="form-control"
                                                    name="{{ $locale }}[name]"
                                                    aria-describedby="basic-addon-name"
                                                    value="{{ old($locale . '.name') }}" required
                                                />
                                                @error($locale . '.name')
                                                <span class="text-danger">{{$message }}</span>
                                                @enderror
                                            </div>
                                        @endforeach

                                    </div>


                                    <div class="row col-12 mb-2">
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label"
                                                       for="exampleFormControlTextarea1">{{__('Description')}}</label>
                                                <textarea
                                                    class="form-control"
                                                    id="exampleFormControlTextarea1"
                                                    rows="3"
                                                    name="description"
                                                    placeholder="{{__('Insert Description')}}"
                                                >{{old('description')}}</textarea>
                                            </div>
                                            @error('description')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <!-- remove thumbnail file upload starts -->
                                        <label class="form-label" for="image">{{ __('Section Image') }}</label>
                                        <div class="card-body">
                                            <input class="form-control" id="uploadInputImage" type="file" name="image"
                                                   accept="image/png, image/gif, image/jpeg"/>
                                            @error('image_url')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                            <img style="width: 129px; margin-top: 2%;border-radius: 10px"
                                                 id="uploadInputPreview"/>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-6">
                                            <div style="margin-left: 3%;" class="mb-2 form-check form-switch">
                                                <input type="checkbox" class="form-check-input" name="active"
                                                       id="customSwitch1" @if(old('active') == 'on') checked @endif/>
                                                <label class="form-check-label"
                                                       for="customSwitch1">{{ __('Activate') }}</label>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                @permission('create_section')
                                <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                                @endpermission
                            </form>
                        </div>
                        <!-- /Vertical Wizard -->
                    </div>
            </div>
    </div>

@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>



@endsection

@include('admin.user.section.include.scripts')




