@foreach ($employees as $index => $employee)
    <tr>
        <td>{{++$index}}</td>
        <td>
            <div class="avatar"><img style="width:30px;height: 30px" src="{{asset($employee->image_url)}}">
            </div> {{$employee->first_name.' '.$employee->last_name}}</td>
        <td>{{$employee->email}}</td>
        <td>{{$employee->phone}}</td>
        <td>{{$employee->section->name_ar}}</td>
        @if(!is_null($employee->roles()->first()))
            @if(app()->getLocale() == 'en')
                <td><i class="fa fa-database text-danger"></i> {{ $employee->roles()->first()->name}}</td>
            @else
                <td>{{$employee->roles()->first()->name_ar}}</td>
            @endif
        @else
            <td>---</td>
        @endif
        <td>@if($employee->active == 1)
                <span
                    class="badge rounded-pill badge-light-success">{{__('Active')}}</span>
            @else
                <span
                    class="badge rounded-pill badge-light-danger">{{__('InActive')}}</span>
            @endif</td>


        <td>
            <div class="dropdown">
                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    @permission('update_employee')
                    <a class="dropdown-item" href="{{route('employee.edit',$employee-> id)}}">
                        <i data-feather="edit-2" class="me-50"></i>
                        <span>{{__('Edit')}}</span>
                    </a>
                    @endpermission


                    @permission('read_employee')
                    <a class="dropdown-item" href="{{route('employee.show',$employee-> id)}}">
                        <i data-feather="eye" class="me-50"></i>
                        <span>{{__('Show')}}</span>
                    </a>
                    @endpermission


                    @permission('delete_employee')
                    <div class="col-4">
                        <form method="post" id="myForm"
                              action="{{route('employee.destroy',$employee-> id)}}">
                            @csrf
                            @method('DELETE')
                            <button class="btn delete"><i data-feather="trash" class="me-50"></i>
                                <span>{{__('Delete')}}</span></button>
                        </form>
                    </div>
                    @endpermission

                </div>
            </div>
        </td>


    </tr>
@endforeach
{{--<tr>--}}
<td colspan="3">
    {!! $employees->appends(request()->query())->links("pagination::bootstrap-4") !!}
</td>
{{--</tr>--}}

<script>
    $(document).ready(function () {
        $(".dropdown-toggle").dropdown();
    });
</script>

<script>
    $(document).ready(function () {

        //delete
        $('.delete').click(function (e) {

            var that = $(this)

            e.preventDefault();

            var n = new swal({
                title: '{{__('Are you sure you want to delete this record?')}}',
                text: "{{__('If you delete this, it will be gone forever.')}}",
                icon: "warning",
                // buttons: true,
                showCancelButton: true,
                showConfirmButton: true,
                dangerMode: true,
                type: "danger",
                closeOnConfirm: false,
                confirmButtonClass: "btn-danger",
            }).then((willDelete) => {
                if (willDelete['isConfirmed'] == true) {
                    that.closest('form').submit();
                } else {
                    // n.close();
                }
            });

        });//end of delete
    });//end of ready

</script>

<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>
