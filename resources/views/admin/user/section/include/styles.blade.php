<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style>

    .row {
        --bs-gutter-x: -2rem !important;
    }

    td {
        white-space: nowrap;
        max-width: 100%;
    }
</style>
