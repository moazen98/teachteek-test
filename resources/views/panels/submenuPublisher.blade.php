@if(app()->getLocale() == 'ar')

    <style>
        .vertical-layout.vertical-menu-modern.menu-expanded .main-menu .navigation li.has-sub > a:after{
            left: 20px;
            right: initial;
        }
    </style>

@endif


{{-- For submenu --}}
<ul class="menu-content">
    @if(isset($menu))
        @foreach($menu as $submenu)
{{--            <li @if($submenu->slug === Route::currentRouteName()) class="active" @endif>--}}
            <li @if($submenu->slug === \Illuminate\Support\Facades\Request::segment(3)) class="active" @endif>
                <a href="{{isset($submenu->url) ? url($submenu->url):'javascript:void(0)'}}"
                   class="d-flex align-items-center"
                   target="{{isset($submenu->newTab) && $submenu->newTab === true  ? '_blank':'_self'}}">
                    @if(isset($submenu->icon))
                        <i data-feather="{{$submenu->icon}}"></i>
                    @endif
                    <span class="menu-item text-truncate">{{ __('locale.'.$submenu->name) }}</span>
                </a>
                @if (isset($submenu->submenu))
                    @include('panels/submenu', ['menu' => $submenu->submenu])
                @endif
            </li>
        @endforeach
    @endif
</ul>
