@foreach ($blogs['blogs'] as $index => $blog)
    <tr>
        <td>{{$blog['id']}}</td>

        <td>{!! $blog['description'] !!}</td>
        <td>{{$blog['image_number']}}</td>

        <td>
            <div class="dropdown">
                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end">

                    <a class="dropdown-item" href="{{route('website.blog.show',$blog['id'])}}">
                        <i data-feather="eye" class="me-50"></i>
                        <span>{{__('Show')}}</span>
                    </a>


                </div>
            </div>
        </td>
    </tr>
@endforeach



<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>
