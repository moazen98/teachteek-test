@extends('layouts.contentLayoutMaster')

@section('title', __('Show blogs'))

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection


@section('content')


    @include('admin.partials.alert')

    <form action="#" method="GET">

        <div class="row">
            <div class="form-group col-4" style="margin-bottom: 10px">
                <input type="text" class="form-control" name="search" id="search"
                       placeholder="{{__('Search By ID , Description')}}"
                       value="{{ request()->input('search') }}">
                <span class="text-danger">@error('search'){{ $message }} @enderror</span>
            </div>

        </div>
    </form>



    <!-- Column Search -->
    <section id="ajax-datatable">

        <div class="row">


            <div class="col-12">


                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">{{__('Blogs Info')}}</h4>


                    </div>


                    <div class="card-datatable overflow-auto">
                        <table class="datatables-ajax table table-responsive ">
                            <thead>
                            <tr>
                                <th>{{__('ID')}}</th>
                                <th>{{__('Description')}}</th>
                                <th>{{__('Images number')}}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>

                            <tbody>
                            @include('customer.blog.pagination_data_index')
                            </tbody>

                        </table>
                    </div>

                    <input type="hidden" name="hidden_page" id="hidden_page" value="1"/>
                    <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="id"/>
                    <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc"/>

                </div>
            </div>
        </div>
    </section>
    <!--/ Column Search -->



@endsection


@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    {{--        <script src="{{ asset(mix('js/scripts/tables/table-datatables-advanced.js')) }}"></script>--}}
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>


    @include('admin.partials.scripts')

    @include('admin.partials.delete')

    @include('customer.blog.filters_index')

@endsection


