@extends('layouts.contentLayoutMaster')

@section('title', __('Show blog'))

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <!-- vendor css files part2 -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/katex.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/monokai-sublime.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.bubble.css')) }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Inconsolata&family=Roboto+Slab&family=Slabo+27px&family=Sofia&family=Ubuntu+Mono&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">

@endsection

@section('page-style')
    {{-- Page Css files --}}


    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection

@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-body">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left card-title">{{ __('General Info') }}</h3>
                            <div class="float-right" @if(app()->getLocale() == 'en')style="margin-left: 600px"
                                 @else style="margin-right: 600px" @endif>

                            </div>


                        </div>
                        <div class="user-avatar-section">
                            <div class="d-flex align-items-center flex-column">
                                <img
                                    class="img-fluid rounded mt-3 mb-2"
                                    src="{{$blog['customer']['image_path']}}"
                                    height="110"
                                    width="110"
                                    alt="User avatar"
                                />
                                <div class="user-info text-center">
                                    <h4>{{$blog['customer']['full_name']}}</h4>
                                    <span class="badge bg-light-info">{{$blog['customer']['job_title']}}</span>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="card-body">


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('First Name') }}:</h4>
                                    <p> {{ $blog['customer']['first_name'] }} </p>
                                </div>
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Last Name') }}:</h4>
                                    <p> {{ $blog['customer']['last_name'] }} </p>
                                </div>
                            </div>


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Phone Number') }}:</h4>
                                    <p> {{ $blog['customer']['phone'] }} </p>
                                </div>

                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Email') }}:</h4>
                                    <p> {{ $blog['customer']['email'] }} </p>
                                </div>
                            </div>

                            {{-- Status & Classification --}}


                            <div class="row mb-2">
                                <div class="col-12">
                                    <h4>{{ __('Description') }}:</h4>
                                    <p> {!! $blog['description'] !!} </p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="content-body">
                        <div class="row">
                            <div class="col-md-12 col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="float-left card-title">{{ __('Post images') }}</h3>
                                    </div>
                                    <br>

                                    <div class="card-body">


                                        @if(!is_null($blog['images']))
                                            <div class="row mb-2">

                                                @foreach($blog['images'] as $attachment)
                                                    @foreach($attachment as $item)

                                                        <div class="col-2">
                                                            <img class="img-fluid card-img-top"
                                                                 style="width: 70px; margin-top: 2%;height: 70px"
                                                                 src="{{asset($item['url'])}}"
                                                                 id="uploadPreview"/>
                                                        </div>

                                                    @endforeach
                                                @endforeach

                                            </div>
                                        @endif

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>

@endsection
@section('page-script')
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>


    @include('admin.partials.scripts')

@endsection
