@extends('layouts/contentLayoutMaster')

@section('title', __('Add blog'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

<head>
    {{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

    <style>

        .image_area {
            position: relative;
        }

        img {
            display: block;
            max-width: 100%;
        }

        .preview {
            overflow: hidden;
            width: 160px;
            height: 160px;
            margin: 10px;
            border: 1px solid red;
        }

        .modal-lg {
            max-width: 1000px !important;
        }

        .overlay {
            position: absolute;
            bottom: 10px;
            left: 0;
            right: 0;
            background-color: rgba(255, 255, 255, 0.5);
            overflow: hidden;
            height: 0;
            transition: .5s ease;
            width: 100%;
        }

        .image_area:hover .overlay {
            height: 50%;
            cursor: pointer;
        }

        .text {
            color: #333;
            font-size: 20px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            text-align: center;
        }

        .choose-position .select2-container--default.select2-container--focus .select2-selection--single {
            border: 1px solid #c9c9c9 !important;
            outline: 0;
        }

        .choose-position span.select2-selection.select2-selection--single {
            outline: none;
        }

        .choose-position .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 35px;
            top: 50%;
            transform: translateY(-50%);
            right: 10px;
            width: 35px;
            background-color: #fff;
            border-radius: 50%;
            display: flex;
            align-items: center;
            justify-content: center;
            box-shadow: 0 5px 20px rgba(35, 49, 45, 0.14);
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            background-image: url(https://cdn4.iconfinder.com/data/icons/user-interface-174/32/UIF-76-512.png);
            background-color: transparent;
            background-size: contain;
            border: none !important;
            height: 20px !important;
            width: 20px !important;
            margin: auto !important;
            top: auto !important;
            left: auto !important;
        }

        .head-sec {
            font-size: 16px;
            font-weight: bold;
        }

    </style>

</head>



@section('content')
    <section id="basic-horizontal-layouts">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('Add Blog to the System')}}</h4>
                </div>


                    <!-- Vertical Wizard -->
                    <div class="card-body">
                        <form action="{{ route('website.blog.store') }}" method="post"
                              enctype="multipart/form-data"
                             >
                            @csrf

                            <div class="row">
                                <div class="mb-2 col-12">
                                    <label class="form-label"
                                           for="selectDefault">{{__('Description')}}</label>
                                    <textarea
                                        class="form-control ckeditor"
                                        id="description"
                                        rows="3"
                                        name="description"
                                        placeholder="{{__('description')}}"
                                        required
                                    >{{old('description')}}</textarea>
                                    @error('description')
                                    <span class="text-danger">{{$message }}</span>
                                    @enderror
                                </div>
                            </div>



                                <div class="row">
                                    <div class="form-group col-4" style="margin-bottom: 10px">
                                        <input type="text" class="form-control" name="search" id="search"
                                               placeholder="{{__('Search By Name , ID')}}">
                                    </div>

                                </div>



                            <!-- Column Search -->
                            <section id="ajax-datatable">

                                <div class="row">


                                    <div class="col-12">


                                        <div class="card">
                                            <div class="card-header border-bottom">
                                                <h4 class="card-title">{{__('Gifs search')}}</h4>



                                            </div>


                                            <div class="card-datatable overflow-auto">
                                                <table class="datatables-ajax table table-responsive ">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('Select')}}</th>
                                                        <th>{{__('ID')}}</th>
                                                        <th>{{__('Image')}}</th>
                                                        <th>{{__('Slug')}}</th>
                                                        <th>{{__('Type')}}</th>
                                                    </tr>

                                                    <tbody>
                                                    @include('customer.blog.pagination_data')
                                                    </tbody>

                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>
                            <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                        </form>
                    </div>
                <!-- /Vertical Wizard -->
            </div>
        </div>
        </div>
    </section>

@endsection
@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    @include('customer.blog.filters')
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>


<script>
    CKEDITOR.config.language = "{{ app()->getLocale() }}";
</script>

