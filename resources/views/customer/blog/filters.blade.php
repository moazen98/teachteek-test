<script>
    $(document).ready(function () {


        function fetch_data(query) {
            $.ajax({
                url: "/customer/blogs/search-gif?query=" + query,
                success: function (data) {
                    $('tbody').html('');
                    $('tbody').html(data);
                }
            })
        }

        $(document).on('keyup', '#search', function () {

            var query = $('#search').val();
            fetch_data(query);
        });


    });
</script>
