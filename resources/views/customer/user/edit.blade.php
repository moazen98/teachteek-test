@extends('layouts.contentLayoutMaster')

@section('title', __('Edit Customer'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">

@endsection





@section('content')
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

    <div class="row">


        <section id="basic-horizontal-layouts">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{__('Edit Customer in the System')}}</h4>
                    </div>
                    <div class="alert-body">
                        <!-- Vertical Wizard -->
                        <div class="card-body">
                            <form action="{{ route('website.customer.update',$customer['id']) }}" method="post"
                                  enctype="multipart/form-data" id="regForm2">
                                @csrf

                                <div class="mb-1">
                                    <div class="row col-12 mb-2">
                                        <div class="col-6">
                                            <label class="form-label" for="basic-addon-name">{{ __('First Name') }}
                                                *</label>
                                            <input
                                                type="text"
                                                id="basic-addon-name"
                                                class="form-control"
                                                name="first_name"
                                                aria-label="Full name"
                                                aria-describedby="basic-addon-name"
                                                value="{{$customer['first_name']}}" required
                                            />
                                            @error('first_name')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-6">
                                            <label class="form-label" for="basic-addon-name">{{ __('Last Name') }}
                                                *</label>
                                            <input
                                                type="text"
                                                id="basic-addon-name"
                                                class="form-control"
                                                name="last_name"
                                                aria-label="last_name"
                                                aria-describedby="basic-addon-name"
                                                value="{{$customer['last_name']}}" required
                                            />
                                            @error('last_name')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row col-12 mb-2">

                                        <div class="col-6">
                                            <label class="form-label"
                                                   for="basic-addon-name">{{ __('Job Title') }}</label>
                                            <input
                                                type="text"
                                                id="basic-addon-name"
                                                class="form-control"
                                                name="job_title"
                                                aria-label="job_title"
                                                aria-describedby="basic-addon-name"
                                                value="{{$customer['job_title']}}"
                                            />
                                            @error('job_title')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="row col-12 mb-2">
                                        <div class="col-6">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="section">{{__('Gender')}}</label>
                                                    <select class="select2 form-select" name="gender">
                                                        <option @if($customer['gender'] == 1) selected @endif
                                                        value="1">{{__('Male')}}</option>

                                                        <option @if($customer['gender'] == 0) selected @endif
                                                        value="0">{{__('Female')}}</option>
                                                    </select>
                                                    @error('gender')
                                                    <span class="text-danger">{{$message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label class="form-label"
                                                   for="basic-addon-name">{{ __('Birthdate') }}</label>
                                            <input
                                                type="date"
                                                id="basic-addon-name"
                                                class="form-control"
                                                name="birthdate"
                                                aria-label="job_title"
                                                aria-describedby="basic-addon-name"
                                                value="{{$customer['birthdate']}}"
                                            />
                                            @error('birthdate')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="row col-12 mb-2">
                                        <div class="col-6">
                                            <label class="form-label" for="phone">{{ __('Phone') }} *</label>
                                            <br>
                                            <input class="form-control" id="phone" name="phone_number" type="tel"
                                                   oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                   value="{{$customer['phone']}}" required>
                                            @error('phone_number')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>

                                        <div class="col-6">
                                            <label class="form-label" for="basic-addon-name">{{ __('Email') }} *</label>
                                            <input
                                                type="email"
                                                id="basic-addon-name"
                                                class="form-control"
                                                name="email"
                                                aria-label="email"
                                                aria-describedby="basic-addon-name"
                                                value="{{$customer['email']}}" required
                                            />
                                            @error('email')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row col-12 mb-2">

                                        <div class="col-6">
                                            <div class="d-flex justify-content-between">
                                                <label class="form-label" for="login-password">{{__('Password')}} (
                                                    <small>{{__(' Fill it when need to change password ')}}</small>)</label>


                                            </div>
                                            <div class="input-group input-group-merge form-password-toggle">
                                                <input
                                                    type="password"
                                                    class="form-control form-control-merge"
                                                    id="password"
                                                    name="password"
                                                    placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                                    aria-describedby="login-password"
                                                />
                                                <span class="input-group-text cursor-pointer"><i
                                                        data-feather="eye"></i></span>
                                            </div>
                                            @error('password')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>

                                        <div class="col-6">
                                            <div class="d-flex justify-content-between">
                                                <label class="form-label"
                                                       for="login-password">{{__('Confirm Password')}} (
                                                    <small>{{__(' Fill it when need to change password ')}}</small>)</label>


                                            </div>
                                            <div class="input-group input-group-merge form-password-toggle">
                                                <input
                                                    type="password"
                                                    class="form-control form-control-merge"
                                                    id="login-password"
                                                    name="confirm_password"
                                                    placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                                    aria-describedby="login-password"
                                                />
                                                <span class="input-group-text cursor-pointer"><i
                                                        data-feather="eye"></i></span>
                                            </div>
                                            @error('confirm_password')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row col-12 mb-2">
                                        <div class="form-group col-md-6">
                                            <label for="PRODUCT_CATEGORY">{{__('Country')}} *</label>

                                            <select id="COUNTRY_CATEGORY" class="form-control select2" name="country"
                                            >
                                                <option disabled selected value>{{__('Select Country')}}</option>
                                                @foreach($countries['countries'] as $country)
                                                    <option value="{{$country['id']}}"
                                                            @if($customer['country'])  @if($customer['country_id'] == $country['id']) selected
                                                            @endif @endif
                                                            name="COUNTRY_ID">{{$country['name']}}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('country'))
                                                <span
                                                    class="text-danger">{{ $errors->first('country') }}</span>
                                            @endif
                                        </div>


                                        <div class="form-group col-md-6">
                                            <label for="CITY">{{__('City')}} *</label>

                                            <select id="CITY" class="select2 form-control" name="city">
                                                @if(count($cities) > 0)
                                                    @foreach($cities['cities'] as $city)
                                                        <option value="{{$city['id']}}"
                                                                @if($city['id'] == $customer['city_id']) selected @endif
                                                        >{{$city['name']}}
                                                        </option>

                                                    @endforeach
                                                @else
                                                    <option
                                                        disabled selected="false">{{__('City')}}
                                                    </option>

                                                @endif
                                            </select>
                                            @if($errors->has('city'))
                                                <span
                                                    class="text-danger">{{ $errors->first('city') }}</span>
                                            @endif
                                        </div>

                                    </div>

                                    <div class="row col-12 mb-2">
                                        <div class="col-12">
                                            <label class="form-label" for="basic-addon-name">{{ __('Address') }}</label>
                                            <input
                                                type="text"
                                                id="basic-addon-name"
                                                class="form-control"
                                                name="address"
                                                aria-label="address"
                                                aria-describedby="basic-addon-name"
                                                value="{{$customer['address']}}"
                                            />
                                            @error('address')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="row col-12 mb-2">
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label"
                                                       for="exampleFormControlTextarea1">{{__('Notes')}}</label>
                                                <textarea
                                                    class="form-control"
                                                    id="exampleFormControlTextarea1"
                                                    rows="3"
                                                    name="note"
                                                    placeholder="{{__('Insert Note')}}"
                                                >{{$customer['note']}}</textarea>
                                            </div>
                                            @error('note')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="row mb-2">
                                        <div class="col-12">
                                            <!-- remove thumbnail file upload starts -->
                                            <label class="form-label" for="image">{{ __('Employee Image') }}</label>
                                            <div class="card-body">
                                                <input class="form-control" id="uploadInputImage" type="file"
                                                       name="image"
                                                       accept="image/png, image/gif, image/jpeg"/>
                                                @error('image')
                                                <span class="text-danger">{{$message }}</span>
                                                @enderror
                                                <img class="img-fluid card-img-top"
                                                     style="width: 129px; margin-top: 2%;height: 100px"
                                                     src="{{$customer['image_path']}}" id="uploadInputPreview"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-6">
                                            <div style="margin-left: 3%;" class="mb-2 form-check form-switch">
                                                <input type="checkbox" class="form-check-input" name="status" value="1"
                                                       id="customSwitch1" @if($customer['active'] == 1) checked @endif/>
                                                <label class="form-check-label"
                                                       for="customSwitch1">{{ __('Activate') }}</label>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                                <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                            </form>
                        </div>
                        <!-- /Vertical Wizard -->
                    </div>
                </div>
            </div>
        </section>

    @section('vendor-script')
        <!-- vendor files -->
            <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
            <!-- vendor files -->
            <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
            <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
            <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

    @endsection
    @section('page-script')
        <!-- Page js files -->
            <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
            <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>


    @include('admin.user.employee.include.phone_script')

    @include('admin.user.employee.include.crud_script')

    @include('admin.partials.scripts')

@endsection


@endsection




