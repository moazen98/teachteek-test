<?php

return [

//    'success' => 'The data created successful',
//    'delete' => 'The data deleted successful',
//    'failed' => 'There is an error',
//    'string' => 'The field must be a string',
//    'update' => 'The data updated successful',
//    'accept' => 'The Order Accepted',
//    'refuse' => 'The Order Refused',
    'success' => 'تم إنشاء البيانات بنجاح',
    'login_success' => 'تم تسجيل الدخول بنجاح',
    'customer_not_found' => 'الحساب غير موجود مسبقا',
    'login_failure' => 'فشل تسجيل الدخول',
    'delete' => 'تم حذف البيانات بنجاح',
    'failed' => 'هناك خطأ قد حدث',
    'string' => 'يجب أن يكون الحقل كلمة',
    'update' => 'تم تعديل البيانات بنجاح',
    'accept' => 'تم قبول الطلب بنجاح',
    'refuse' => 'تم رفض الطلب بنجاح',
    'upload_successful' => 'تم تحميل البيانات بنجاح',
    'upload_failed' => 'فشل تحميل البيانات',
    'login_failed' => 'فشل عملية تسجيل الدخول',
    'logout_success' => 'تم تسجيل الخروج بنجاح',
    'created_success' => 'تم إنشاء البيانات بنجاح',
    'created_failed' => 'فشل إنشاء البيانات',
    'old_password' => 'كلمة السر غير متطابقة',
    'password_wrong' => 'كلمة السر غير صحيحة',

];
